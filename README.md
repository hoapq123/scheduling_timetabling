# [LINGI2990] Course Scheduler 

This project solve the scheduling of the course timetabling problem for the EPLs masters scheduling problem  
The solver takes into account :  
 1. program.txt   	: A set of courses which each student group has to take  
 2. course.txt  		: The number of lecture and exercise sessions of each courses.   
 3. group.txt 		: Number of students registered for each program.  
 4. ProfCourse.txt 	: A set of professors who are responsible for each course  
 5. ProfSlot.txt 		: The strict unavailable slots and non-preferred slots of each professor    
  
It then tries to find a high-quality timetable.

# Dependencies: 
* [OsCAR](https://bitbucket.org/oscarlib/oscar/wiki/Home)

This is a tool used to schedule course timetabling.

Dependencies
  - Oscar