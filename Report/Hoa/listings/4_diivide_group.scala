	// each group has an object containing all information and variables of subgroups
	val lstGroupObj = Map[String,GroupInfo]()
	/*
     * Class contains all information of group
     * 	- Divide into subgroups
     *  - Initialize subgroup partial exercise variable
     */
    class GroupInfo(val groupName: String,val totStudent: Int, val nbSubGroup : Int ){
         /* each subgroup has a map with key as courseID and value as its partial exercise variable */
        val subGroupExerciseIndex = Array.fill(nbSubGroup)(Map[String,CPIntVar]())
		
		...
		
	}