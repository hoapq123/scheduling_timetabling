	/* Step 2: After having the first solution
	 *		  Use branch and bound approach to find the optimal solution */
	 
	/* Restarts */ 
	while(condition) {
		lastSolutionStat = startSubjectTo(failureLimit = limitedBacktrack){
			// get a set of courses having no conflict are fixed
			val noConflictCourses = getNonConflictCourses()
			
			// a portion of courses having no conflict are fixed
			for(course <- noConflictCourses; if rand.nextInt(100) < fixPercent ){
				// fixed slot lecture
				slotLecture(course).foreach{ x =>  add(x == cp.lastSol(x)) }
				
				// fixed slot exercise
				if(slotExercise.contains(course) )
					slotExercise(course).foreach{ x => add(x == cp.lastSol(x)) }
				
				// fixed subgroups in each partial subscription exercise sessions
				if(slotPartialSubcriptionExercise.contains(course))
					  subgroupExerciseVariable(course).map{x => add(x == cp.lastSol(x)) }
			}
		} // end startSubjectTo
	}	