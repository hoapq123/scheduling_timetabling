	/*
    * C2: A professor can not teach 2 different sessions at the same time
    */
    for(p <- professors){
        val sessionOfProfessor = professorSchedule.getOrElse(p, Array())
        if(sessionOfProfessor.size > 1){
            add(allDifferent(sessionOfProfessor))
        }
    }