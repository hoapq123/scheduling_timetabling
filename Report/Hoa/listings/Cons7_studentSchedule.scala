	val subGroupViolation = Map[(String,Int),CPIntVar]()
    
    /* C8: Calculate the conflict schedule of each subgroup */
    for(group <- groupStudent; i <- 0 until nbSubGroups(group)){
        add(softAllDifferent(subGroupSchedule(group,i), subGroupViolation(group,i)) )
    }