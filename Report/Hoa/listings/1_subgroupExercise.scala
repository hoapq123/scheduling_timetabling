	
	/*
     * GroupInfo is a Class contains all information of group
     * 	- Divide into subgroups
     *  - Initialize  partial exercise variable of subgroup
     */
	val subGroupExerciseIndex = Map[String,GroupInfo]()
	
	/* all partial exercise variable of subgroup */
	subGroupExerciseIndex(group)(i) : Map[String,CPIntVar]()