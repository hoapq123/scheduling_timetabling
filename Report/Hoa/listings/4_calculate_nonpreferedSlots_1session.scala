	/* Calculate the conflict in non-preferred slots of each  professor */
	for(session <- professorSchedule){
		...	
		/* check whether the session is in non-preferred slots */
		val isInNonPreferredSlot = sum(0 until nonPreferredSlots.size)
														(i => session === nonPreferredSlot(i)))
		...
	}