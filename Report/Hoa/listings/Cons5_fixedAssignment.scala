	/*
     * C5: fixed lecture and exercise slots of courses 
     */
    for((course,fixedSlots) <- data.fixedLectureOfCourse){
        for(i <- 0 until math.min(fixedSlots.size,data.nbLecture(course)) ){
            add(slotLecture(course)(i) ==  fixedSlots(i))
        }
    }
    