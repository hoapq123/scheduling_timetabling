	// VO-LNS configuration
	val allObjectives = Array[CPIntVar](objective1, 
									objective2,objective3,objective4,objective5)
	solver.minimize(allObjectives)
	
	// minimizing the objective1 
	solver.obj(objective1).tightenMode = TightenType.StrongTighten
	for(obj <- allObjectives; if obj != objective1)
      solver.obj(obj).tightenMode = TightenType.NoTighten
	  
	...  
	// Restarts
	while (condition) {
        lastSolutionStat = startSubjectTo(failureLimit = limitedBacktrack){   
			// change to the optimization of objective2
			if(changeObjectiveCondition){
				solver.obj(objective1).tightenMode = TightenType.WeakTighten
				solver.obj(objective2).tightenMode = TightenType.StrongTighten
			}
		}