	/*
     * C7: Continuous sessions
     */
    for(course <- data.continuousCourses){
		// course has one lecture and at least one exercise session
        if(data.nbLecture(course) == 1 && data.nbExercise(course) >= 1){
            add(table(slotLecture(course)(0), slotExercise(course)(1), continousSlots))
        }
		// course has two lecture sessions
		else if(data.nbLecture(course) == 2){		
            add(table(slotLecture(course)(0), slotLecture(course)(1), continousSlots))
		}
    }