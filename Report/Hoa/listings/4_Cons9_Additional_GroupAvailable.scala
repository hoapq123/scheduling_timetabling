	/* 
	 * Array stores the information of availability of each group in a specific slot
	 * If groupAvailable(gId,s) =  1 then group has number gId is free in slot s
	 */
	val groupAvailable: Array(nbGroup,nbSlots) 
	
	/* Remove invalid value from the domain of variable of partial subscription exercise */
	for((course,vars) <- slotPartialExercise){
		val minStudentPerSlot = (avgStuPerExerciseSession(course) * margin )
		
		// get value in domain of all variables of slot exercise
		val allValue = scala.collection.mutable.Set[Int]()
		for(x <- vars; if !x.isBound){ x.map { v => allValue.add(v) } }
	  
		// for each slot in domain, if the total student available is smaller 
		// than the minimum required, then remove this value in the domain
		for(v <- allValue){  
			var totalStudentAvailable : Int
			
			...
		  
			if(totalStudentAvailable < minStudentPerSlot){
				// remove value in the domain
				for(x <- vars; if x.hasValue(v) ){
					if (x.removeValue(v) == Failure) return Failure
				}
			}
		}
	}