	// binPacking restricts the minimum number of students in exercise sessions of a course
	add(binPacking(varOfExerciseSubGroup, nbStuInSubGroup, loads))
	...
	// the deviation of exercise sessions of a course
	val diffOfSessions = loads.map { x => 
											absolute(x - avgStuPerExerciseSession(course))}
	