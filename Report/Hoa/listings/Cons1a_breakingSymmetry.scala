	/*
     * C6: Break symmetry between lecture sessions or exercise sessions of the same course
     */
    for((course,lectureSessions) <- slotLecture){
        val from = data.fixedLectureOfCourse.getOrElse(course, Array()).size
        // all the non-fixed lecture sessions of course
        if( lectureSessions.size - from > 1)
            for(i <- from until lectureSessions.size -1){
                add(lectureSessions(i) < lectureSessions(i+1))
            }
    }
    
           