	/*
    * C1: Lecture and exercise sessions of the same course are in different slots
    */
    for(c <- courses){
        val allSessionOfCourse = slotLecture(c) ++ slotExercise.getOrElse(c, Array())
        if(allSessionOfCourse.size > 1)
            add(allDifferent(allSessionOfCourse))
    }