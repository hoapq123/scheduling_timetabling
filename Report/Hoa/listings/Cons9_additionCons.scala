	/*
     * Additional constrain: Removing the slots in which number of available students 
     * 										is smaller than the expected minimum 
     */
    add(TTSConstraint(slotLecture,slotMultipleExercise,slotUniExercise, nbStudent,
                data.courseInProgam, avgStuPerExerciseSession, marginStudentInExercise))