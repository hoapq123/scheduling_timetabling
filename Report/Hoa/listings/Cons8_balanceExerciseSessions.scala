	/*
     * C9: Balancing between exercise session :
     * 		Require the minimum number of students attending each exercise sessions of a course		
     */	 
	for(course  <- partialSubscriptionExercise){
		...
		// the minimum number of students 
        val minStudentPerSlot = (avgStuPerExerciseSession(course) * RelaxMargin).toInt          
        
		// the number of students in each sessions are stored in loads array      
        val loads = Array.fill(nbExercise)(CPIntVar(minStudentPerSlot to maxStudentPerSlot))
        
		// all variables of subgroup in partial subscription exercise sessions
        val varOfExerciseSubGroup : Array[CPIntVar]()
		// the number students in the corresponding subgroup
        val nbStuInSubGroup : Array[Int]()
        
        ...

        add(binPacking(varOfExerciseSubGroup, nbStuInSubGroup, loads))
    }