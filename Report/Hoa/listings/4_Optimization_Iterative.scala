	// start from the lower bound
	var lowerBound= totalConflictSchedule.min
	
	// stop searching when finding one solution  or the time execution runs out
	while(!isFound && java.lang.System.currentTimeMillis() - startTime < maxTime){
		val stat = startSubjectTo(nSols = 1, failureLimit = limitedBacktrack){
		   add(totalConflictSchedule <= lowerBound)
		}
		isFound = stat.nSols == 1
		// increase the lower bound when not found the solution
		if(!isFound){                  
			nbRestart += 1
			lowerBound += 1
		}
	}