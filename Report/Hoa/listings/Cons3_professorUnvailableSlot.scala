	/*  C3: No lecture or exercise occurs in the unavailable slots of the responsible professors */
    for((p,unvailableSlot) <- data.unavailableTimeOfProf){
        val sessionOfProfessor = professorSchedule.getOrElse(p, Array())
        for(sessionVar <- sessionOfProfessor)
            unvailableSlot.map(un => add(sessionVar != un))
    }