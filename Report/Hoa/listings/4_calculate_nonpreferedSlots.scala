	/* 
	 * key : ProfessorId
	 * value : an array of variables for checking his sessions are in  non-preferred slots 
	 */
	val conflictNonPrefferedSlotsOfProfessors = Map[String,Array[CPIntVar]]()
	
	/* Calculate the conflict in non-preferred slots of each professor */
    for(p <- professors){
		// set of non-preferred slots of each professor
		val nonPreferredSlot : Set[Int] 
		
		/* Calculate the conflict in non-preferred slots of each teaching unit of professor */
        for(session <- professorSchedule){			
			/*
             * Teaching session can be whether in non-preferred slots 
             * Therefore the value is either 0 or 1
             */
            val isInNonPreferredSlot = CPIntVar(0 to 1)
            
			/* check whether the session is in non-preferred slots */
            add( isInNonPreferredSlot ==  sum(0 until nonPreferredSlot.size)(i => session === nonPreferredSlot(i)))
			
			...
		}
	
	}