	// calculate the value of each lecture session 
	val valueLectureSessions = slotLecture.flatMap{ case (course,vars) 
                => vars.map { session => qualitySlots(session) * totStuLectureSession(course) } }
	
	// calculate the value of each exercise session 	
	val valueExerciseSessions = slotExercise.flatMap{ case (course,vars) 
					=> vars.foreach { session => qualitySlots(session) * avgStuPerExerciseSession(course) } }					
    
	// total value of lecture sessions and exercise sessions
    val valueLecture = sum(valueLectureSessions)
	val valueExercise = sum(valueExerciseSessions)
	
	val objective3 = - valueLecture * 2 - valueExercise