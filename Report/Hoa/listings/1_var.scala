	/* Course sessions */
    val slotLecture = Map[String, Array[CPIntVar]]()
    val slotExercise = Map[String, Array[CPIntVar]]()
	  
	/* Professor schedule */ 
    val professorSchedule = Map[String,Array[CPIntVar]]()
	
	/* Subgroup schedule */
	val subGroupSchedule = Map[(String,Int), Array[CPIntVar]]()
	
	
	