\select@language {english}
\contentsline {chapter}{Abstract}{1}{chapter*.1}
\contentsline {chapter}{Introduction}{7}{chapter*.5}
\contentsline {subsubsection}{Problem}{7}{section*.6}
\contentsline {subsubsection}{Motivation}{8}{section*.7}
\contentsline {subsubsection}{Approach}{8}{section*.8}
\contentsline {subsubsection}{Contribution}{8}{section*.9}
\contentsline {subsubsection}{The structure of the thesis}{8}{section*.10}
\contentsline {chapter}{\numberline {1}Course Timetable Problem}{9}{chapter.1}
\contentsline {section}{\numberline {1.1}University timetabling}{9}{section.1.1}
\contentsline {section}{\numberline {1.2}Introduction Course Timetabling}{10}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Common requirements}{10}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Different goals of stakeholders}{10}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Course structure}{11}{subsection.1.2.3}
\contentsline {subsection}{\numberline {1.2.4}State of art}{11}{subsection.1.2.4}
\contentsline {chapter}{\numberline {2}Constraint Programming}{13}{chapter.2}
\contentsline {section}{\numberline {2.1}Constraint Programming}{13}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Constraint Definition}{13}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Constraint Satisfaction Problem}{13}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Progagation }{14}{subsection.2.1.3}
\contentsline {subsubsection}{Level of Consistency}{14}{section*.11}
\contentsline {subsubsection}{Propagation}{15}{section*.12}
\contentsline {subsection}{\numberline {2.1.4}Search}{15}{subsection.2.1.4}
\contentsline {subsubsection}{Branching strategy}{15}{section*.13}
\contentsline {subsection}{\numberline {2.1.5}Constraint Optimization Problem}{16}{subsection.2.1.5}
\contentsline {section}{\numberline {2.2}Global Constraint}{17}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}AllDifferent}{17}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Global Cardinality Constraint (GCC)}{18}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Bin-Packing Constraint}{18}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Table constraint}{19}{subsection.2.2.4}
\contentsline {chapter}{\numberline {3}Model}{20}{chapter.3}
\contentsline {section}{\numberline {3.1}Problem description}{20}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}The semester}{20}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}The group students and program}{21}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}The course}{22}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}Subgroups}{23}{subsection.3.1.4}
\contentsline {subsection}{\numberline {3.1.5}Professors}{24}{subsection.3.1.5}
\contentsline {subsection}{\numberline {3.1.6}The slots}{24}{subsection.3.1.6}
\contentsline {subsection}{\numberline {3.1.7}The problem parameters}{25}{subsection.3.1.7}
\contentsline {section}{\numberline {3.2}Variables and Domain}{26}{section.3.2}
\contentsline {section}{\numberline {3.3}Constraints}{27}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Constraint 1: No conflict in sessions of each course}{27}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Constraint 2: No conflict in the schedule of professors}{27}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Constraint 3: No teaching session in unavailable slots of professors}{27}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}Constraint 4: Maximum capacity for each slot}{27}{subsection.3.3.4}
\contentsline {subsection}{\numberline {3.3.5}Constraint 5: Assignment sessions}{28}{subsection.3.3.5}
\contentsline {subsection}{\numberline {3.3.6}Constraint 6: Breaking symmetry between sessions}{28}{subsection.3.3.6}
\contentsline {subsection}{\numberline {3.3.7}Constraint 7: Courses having continuous sessions}{28}{subsection.3.3.7}
\contentsline {subsection}{\numberline {3.3.8}Constraint 8: No conflict in student schedule}{28}{subsection.3.3.8}
\contentsline {subsection}{\numberline {3.3.9}Constraint 9: Balance between exercise sessions}{29}{subsection.3.3.9}
\contentsline {section}{\numberline {3.4}Optimization}{29}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Objective 1: Minimizing the total conflicts in student schedule}{29}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Objective 2: Minimizing the total conflicts in non preferred slots of professor schedule}{30}{subsection.3.4.2}
\contentsline {subsubsection}{Sub-Objective 2.1 - Minimizing the total conflicts in non preferred slots of all professors}{30}{section*.14}
\contentsline {subsubsection}{Sub-Objective 2.2 - Minimizing the maximum number of conflicts of a professor}{30}{section*.15}
\contentsline {subsection}{\numberline {3.4.3}Objective 3: Maximizing the quality of schedule}{30}{subsection.3.4.3}
\contentsline {subsection}{\numberline {3.4.4}Objective 4: Minimizing the total number of students attending the end slot of day}{31}{subsection.3.4.4}
\contentsline {subsection}{\numberline {3.4.5}Objective 5: Minimizing the total deviation of number of students exercise sessions}{32}{subsection.3.4.5}
\contentsline {chapter}{\numberline {4}The Solver}{34}{chapter.4}
\contentsline {section}{\numberline {4.1}The variables}{34}{section.4.1}
\contentsline {section}{\numberline {4.2}Constraints}{35}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Constraint 1: No conflict in sessions of each course}{35}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Constraint 2: No conflict in the schedule of professors}{35}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Constraint 3: No teaching session in unavailable slots of professors}{35}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Constraint 4: Maximum capacity for each slot}{36}{subsection.4.2.4}
\contentsline {subsection}{\numberline {4.2.5}Constraint 5: Assignment sessions}{36}{subsection.4.2.5}
\contentsline {subsection}{\numberline {4.2.6}Constraint 6: Breaking symmetry between sessions}{36}{subsection.4.2.6}
\contentsline {subsection}{\numberline {4.2.7}Constraint 7: Courses having continuous sessions}{37}{subsection.4.2.7}
\contentsline {subsection}{\numberline {4.2.8}Constraint 8: No conflict in student schedule}{37}{subsection.4.2.8}
\contentsline {subsection}{\numberline {4.2.9}Constraint 9: Balance between exercise sessions}{37}{subsection.4.2.9}
\contentsline {subsection}{\numberline {4.2.10}Constraint 10: Additional constraint}{38}{subsection.4.2.10}
\contentsline {section}{\numberline {4.3}Division student groups}{39}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Organizing students in subgroups reduces the execution time of finding the solution}{39}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Organizing students in larger units leads to an imbalance of the number of students between exercise sessions}{40}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}How to divide student groups}{40}{subsection.4.3.3}
\contentsline {subsubsection}{Students taking the same program often have the same timetable}{40}{section*.16}
\contentsline {subsubsection}{Proposal of division of student groups}{41}{section*.17}
\contentsline {section}{\numberline {4.4}Optimization techniques}{42}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Large Neighborhood Search}{42}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Variable Objective Large Neighborhood Search (VO-LNS)}{44}{subsection.4.4.2}
\contentsline {section}{\numberline {4.5}Optimization Objectives}{45}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Objective 1: Minimizing the total conflict in student schedule}{45}{subsection.4.5.1}
\contentsline {subsubsection}{Branch and Bound}{46}{section*.18}
\contentsline {subsubsection}{Iterative Optimization}{48}{section*.19}
\contentsline {subsubsection}{Combining both approaches}{49}{section*.20}
\contentsline {subsection}{\numberline {4.5.2}Objective 2: Minimizing the conflicts in non-preferred slots of professors}{50}{subsection.4.5.2}
\contentsline {subsubsection}{Generate non-preferred slots of each professor}{50}{section*.21}
\contentsline {subsubsection}{Calculate the conflicts in non-preferred slots}{50}{section*.22}
\contentsline {subsubsection}{Search}{51}{section*.23}
\contentsline {subsubsection}{Relaxation}{52}{section*.24}
\contentsline {subsection}{\numberline {4.5.3}Objective 3: Maximizing the quality of schedule}{52}{subsection.4.5.3}
\contentsline {subsubsection}{Definition of the third objective}{52}{section*.25}
\contentsline {subsubsection}{Relaxation}{52}{section*.26}
\contentsline {subsubsection}{Result}{52}{section*.27}
\contentsline {subsection}{\numberline {4.5.4}Objective 4: Minimizing the total number of students attending the end slot of day}{53}{subsection.4.5.4}
\contentsline {subsubsection}{Definition of the fourth objective}{53}{section*.28}
\contentsline {subsubsection}{Relaxation}{53}{section*.29}
\contentsline {subsubsection}{The reason to introduce the fourth objective}{54}{section*.30}
\contentsline {subsection}{\numberline {4.5.5}Objective 5: Minimizing the total deviation of number of students in exercise sessions}{54}{subsection.4.5.5}
\contentsline {subsubsection}{Relaxation}{55}{section*.31}
\contentsline {subsubsection}{Result}{55}{section*.32}
\contentsline {subsection}{\numberline {4.5.6}Relaxation}{55}{subsection.4.5.6}
\contentsline {subsubsection}{Worst Solution Relaxation}{55}{section*.33}
\contentsline {subsubsection}{Adaptive Neighborhood Relaxation}{56}{section*.34}
\contentsline {subsection}{\numberline {4.5.7}Search}{57}{subsection.4.5.7}
\contentsline {subsubsection}{Binary First Fail - Dom heuristic}{57}{section*.35}
\contentsline {subsubsection}{Binary First Fail - Dom/deg heuristic}{58}{section*.36}
\contentsline {chapter}{\numberline {5}The experiments}{59}{chapter.5}
\contentsline {section}{\numberline {5.1}Data size}{59}{section.5.1}
\contentsline {section}{\numberline {5.2}Results}{59}{section.5.2}
\contentsline {chapter}{Conclusion}{61}{chapter*.37}
