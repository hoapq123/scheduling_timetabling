\chapter{The Solver}
The previous chapter described the details of model, including the variables, their domains and all the objectives of the problem. This chapter presents the way this model is implemented in \textit{Oscar solver}.

\section{The variables}
%TODO : consider to add subgroupSchedule 
\lstinputlisting{listings/1_var.scala}
All the variables are CPIntVar type and their domains are possible slots of a week (the \textit{rSlots} range of the previous chapter).

Each course in the programs has two parts: lecture theory and exercise sessions. 
\textbf{Lecture theory} information is stored in \textit{slotLecture} map with the courseID as key and an array of the variables as value. The size of each array  is the number of lecture sessions of its corresponding course. Similarly, courses having \textbf{exercise sessions} are represented in \textit{slotExercise} map with courseID as key and exercise sessions stored in an array of the variables as value. 

\textbf{professorSchedule} represents the teaching schedule for each professor. It is a map with professorID as key, and an array of the variables as value. This array represents all teaching sessions of a professor, including both lecture and exercise sessions. To get the schedule of each professor, we  just refer to the created variables of his responsible courses.

\textbf{subGroupSchedule} represents the weekly studying timetable for several students having same schedule. This is a map with key composed of the group name and the index of the subgroup in the parent group. Value of the map represents all variables of courses which the subgroup enrolled. 
In lecture sessions and full subscription exercise sessions,  all registered students have to fully attend, so we  refer to created variables (\textit{slotLecture}, \textit{slotExercise} variables). However, if a course has the partial subscription exercise part, a new variable is created to represent the index of the exercise session at which the subgroup attends. All these variables are stored in the \textit{subGroupExerciseIndex} map. The domain of these variables is the possible exercise sessions of the course which subgroups could take (range from 0 until $NbExercise_{course}$).

%TODO example of subgroupSchedule


\section{Constraints}
\subsection{Constraint 1: No conflict in sessions of each course}
We set \textit{allDifferent} constraint on all variables of a course. 
\lstinputlisting{listings/Cons1_courseConflict.scala}

\subsection{Constraint 2: No conflict in the schedule of professors}
We also use \textit{allDifferent} constraint on all variables of a professor schedule to ensure that he is only in charge of one teaching unit for each slot.
\lstinputlisting{listings/Cons2_professorSchedule.scala}

\subsection{Constraint 3: No teaching session in unavailable slots of professors}
We iterate over all variables  of a professor schedule and make sure that they are not in his unavailable slots. These invalid values of variables of teaching sessions can be removed in the initialization of variables. However, we decided  to model this requirement separately so that the code can be modularized.
\lstinputlisting{listings/Cons3_professorUnvailableSlot.scala}

\subsection{Constraint 4: Maximum capacity for each slot}
For each slot, the number of teaching sessions does not exceed the given maximum number of activities.  \textit{GCC} constraint is used to model this requirement
\lstinputlisting{listings/Cons4_maxActivities.scala}

\subsection{Constraint 5: Assignment sessions} 
This constraint assigns each fixed slot to the teaching session. We also ensure that the number of fixed slots does not exceed the number of lecture sessions of the course. The fixed exercise sessions are processed in the same way.
\lstinputlisting{listings/Cons5_fixedAssignment.scala}

\subsection{Constraint 6: Breaking symmetry between sessions}
In a course having two lecture sessions, if  the values of the two 
slots are swapped then the search considers it a new solution although the schedule of course does not change. To avoid variable symmetry, we added a constraint that forces the lecture slots to be strictly increasing. Notice that, this additional constraint is only enforced on non-fixed variables. Breaking symmetry of exercise variables is similar so we do not present the code here.

\lstinputlisting{listings/Cons1a_breakingSymmetry.scala}

\subsection{Constraint 7: Courses having continuous sessions}
 This requirement is modeled by using \textit{Table} constraint. In this constraint, \textit{continousSlots} is a table listing continuous slot patterns. There are two cases of continuous slots: the course has two lecture sessions or the course with one lecture and at least one exercise session. Two cases are checked and the corresponding variables are set.
\lstinputlisting{listings/Cons6_continuousCourses.scala}

\subsection{Constraint 8: No conflict in student schedule}
\label{cons8_conflict}
One of the most important requirements of the course timetabling problem is that there is no conflict in student schedule. However, with some data instances, this requirement makes the problem over-constrained so it is considered as a soft constraint. The violation of each subgroup schedule is stored in \textit{subGroupViolation} variable
\lstinputlisting{listings/Cons7_studentSchedule.scala}

\subsection{Constraint 9: Balance between exercise sessions} \label{cons9_balance}
The numbers of students in exercise sessions of the same course need to be balanced. However, this requirement is often too strong for realistic instances so it was relaxed. It only requires that the ratio of  students attending in each exercise sessions to the average of number of students  is at least equal to \textit{RelaxMargin}.

This requirement is described by using a \textit{binPacking} constraint since the arrangement of subgroups into exercise sessions is similar to put items into bins. The minimum number of students attending each exercise session of a course is calculated in line 8. This number is also the lower bound of the bins (line 11). The exercise variables of all subgroups attending this course are stored in \textit{varOfExerciseSubGroup} array.
 The  number of students of  each subgroup is stored in \textit{nbStuInSubGroup} array and it is considered as the weight of each item in the bin packing problem (line 16).

\clearpage

\lstinputlisting[numbers=left]{listings/Cons8_balanceExerciseSessions.scala}

\subsection{Constraint 10: Additional constraint}
The previous constraint ensures that the number of students in each partial subscription exercise reaches the minimum. However, \textit{binPacking} constraint is only trigged when the variables of the index of subgroup in  exercise session are assigned (\textit{subGroupExerciseIndex} variables). However, in some cases where a exercise session is possibly assigned to a slot at which there is not 	enough number of attending students.
\begin{exmp}
	Course A  has two sessions but it requires each registered student to attend one of them. There are four groups (g1,g2,g3,g4) taking this course. In slot 2, g1 has a lecture of course B while g2 takes course C \ref*{tab:cons10_groupAvailable}. Only two groups, g2 and g4 can participate the exercise of course A, if its session is scheduled to slot 2. Parameter Margin is 0.8, so the exercise session of course A needs at least 16 students. (0.8 * 20 = 16). Therefore, the variable of exercise sessions of course A should remove value 2 because only 7 students can attend this slot.
	
	\begin{table}[!h]
		\centering	
		\begin{tabular}{|l|B|B|B|B|B|B|B|B|}
		\hline
		Slot & 0 & 1 & 2            & 3 & 4 & .. & 19 \\ \hline
		g1: 20 stu   &   &   & Lec: $B^{1}$ &   &   &    &    \\ \hline
		g2: 3 stu   &   &   &   v           &   &   &    &    \\ \hline
		g3: 13 stu   &   &   & Lec: $C^{1}$ &   &   &    &    \\ \hline
		g4: 4 stu   &   &   &    v          &   &   &    &    \\ \hline
		\end{tabular}
		\caption{Example of the availability of groups. The variables of exercise sessions \\ need to remove the slots at which do not have enough attending
			 students}
		\label{tab:cons10_groupAvailable}
	\end{table}
\end{exmp}
In order to remove the invalid value early,  an extra constraint was added. 
This one removes the values of the variables of partial subscription exercises which do not have enough attending students. It is triggered when all lecture sessions and full subscription exercise are bounded and before the variables of partial subscription exercise courses are assigned. The detail of the code of this constraint is presented in the appendix. 

After the pruning of this constraint, the number of available students in all values of the domain of the exercise variables is greater or equal to the minimum requirement. 

\input{chapters/4_model_subgroup.tex}

\input{chapters/4_solver_exploration.tex}

\input{chapters/4_solver_optimization.tex}

\input{chapters/4_search.tex}
