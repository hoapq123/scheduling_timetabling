\section{Division student groups} \label{sec:subgroup}
From the previous chapter, we know that student groups need to be divided into smaller units, which contain several students having the same schedule in both lecture and exercise sessions. These units can be arranged into multiple sessions of partial subscription exercise courses. There are two approaches to split a large group. It can be divided into atomic units (individual students) or subgroups  (coarse-grained level). In this section,  the benefits and drawbacks of two levels of granularity are presented.


\subsection{Organizing students in subgroups reduces the execution time of finding the solution}
In data instances of EPL's problem, each course usually has 1 lecture session and 2 exercise sessions which requires students to attend one of them. Each program has about 6 courses, so in total, 18 sessions need to be arranged for each program. There are 20 slots in the weekly timetable, so the density of sessions is dense. In addition, different programs have several courses in common. As a result, teaching sessions in a same program can overlap each other, especially exercise sessions of different courses. In this case, the scheduling tool needs to find a correct way to arrange subgroups to minimize the total conflict in the student schedule and also ensure the equal number of students attending in  exercise sessions of the same course. In this situation, grouping students into subgroups can make the execution time faster than organizing individual students. The reason is that the search algorithm does not need to compute and verify the permutation of the $n$ students in each subgroup. 

%The reason is that during the search, the change of subgroup variables is equivalent to n times changes of each student (with n is the number students of subgroup).
\begin{exmp}
	Once the schedule of lecture and exercise sessions is planned,  subgroups are arranged into suitable exercise slots. Suppose three courses X,Y,Z are partial subscription exercise and their exercise sessions are scheduled as shown in the following table. Assume that subgroup A has 15 students who enrolled on three courses. During the search process, all exercise sessions of group A are assigned to slot 2. The solver detects the conflicts and tries to reduce it. It moves group A to the second session  of course Y  (slot 5). If students were organized separately then it would need to make 15 times changes to swap students to the second session. 
	\label{exp:exercise_subgroup}
\end{exmp}

\begin{table}[!h]
	\centering	
	\begin{tabular}{CCCCCCCC}
		slot                           & 0                     & 1                     & 2                                & 3                     & 4                                & 5                                & 6                                \\ \hline
		\multicolumn{1}{|l|}{Course X} & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{ex: $X^{1}$} & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{ex: $X^{2}$} & \multicolumn{1}{l|}{}            & \multicolumn{1}{l|}{}            \\ \hline
		\multicolumn{1}{|l|}{Course Y} & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{ex: $Y^{1}$} & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{}            & \multicolumn{1}{l|}{ex: $Y^{2}$} & \multicolumn{1}{l|}{}            \\ \hline
		\multicolumn{1}{|l|}{Course Z} & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{ex: $Z^{1}$} & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{}            & \multicolumn{1}{l|}{}            & \multicolumn{1}{l|}{ex: $Z^{2}$} \\ \hline
	\end{tabular}
	\caption{Choosing the schedule for subgroup A taking three courses X,Y,Z}
	\label{tab:choose_groupSchedule}
\end{table}
Furthermore, the clustering students into subgroups  decreases the number of variables and the number of constraints. As a result, the execution time can be improved.

\subsection{Organizing students in larger units leads to an imbalance of the number of students between exercise sessions}
Despite the previously mentioned advantage, gathering students in larger units can provoke an imbalance between exercise sessions.

\begin{exmp}
	A course has two exercise sessions and requires enrolled students to take only one of them. There are 15 people taking this course. As shown in figure \ref{fig:deviation_ex}, grouping students by groups of 5 people has the minimum deviation of 5 whereas considering individuals allows to reduce that to 1.
	\begin{figure}[!h] 
		\centering
		\includegraphics[scale=0.8]{images/deviationSession.PNG}
		\caption{The deviation of two exercise sessions when organizing students in subgroups.}
		\label{fig:deviation_ex}
	\end{figure}
\end{exmp}

%This is also the reason a large group needs to be split across several smaller units. 
It is important to find a suitable strategy to split groups, limiting the drawback of grouping students in large units while still keeping the benefit of that strategy.

\subsection{How to divide student groups}
\subsubsection{Students taking the same program often have the same timetable}
We know that students in the same group can take different slots of the exercise part of a course. Therefore, if a group has many partial subscription exercises, it possibly has many arrangements for student schedules. For example, considering a group enrolls on \textit{n} courses, each one has two exercise sessions and only requires students to take one of them. In theory, this group has  $2^{n}$ combinations of time schedule. However,  in practice, students registering for the same program are likely to take the same timetable so each group just has a few number of schedules. 

\begin{exmp}\footnote{This example is inspired by the paper of Michael W. Carter \cite{section_group}}
	Suppose 4 students sign up for the course LINMA1691, Discrete Mathematics.  This course has two exercise sessions, but students need to attend only one. There are 2 students learning electricity (ELEC) and 2 students in physic (PHY) major. Each group registers for four other courses. The student schedule can be visualized as a graph. In this graph, course sessions are vertices and there is an edge between two vertices if there is at least one student taking both sessions. The course timetabling problem is reduced to the graph coloring problem.
	
	\begin{figure}[!h] 
		\centering
		\includegraphics[scale=0.8]{images/courseGroup.PNG}
		\caption{When each student group is assigned to separate exercise sessions,  each group has 10 edges for their courses, then there the total edges is 20.}
		\label{groupSplit_diagram}
	\end{figure}
	
	If each group takes in one exercise session, then there are 10 edges for a group in the graph (Figure \ref{groupSplit_diagram}). Meanwhile, if we mix students of different groups and put one electricity student and  one physic student in each session, then the number of edges doubles. When the number of edges in the graph is increased, it becomes much more difficult to find the conflict-free solution of graph coloring problem. It also means finding a solution to the course timetabling problem which has no conflict in the student schedule becomes more difficult.
	
\end{exmp}

In summary, students taking the same set of courses are likely to take the same timetable. Therefore, it is proposed that only large  groups need to be divided to ensure the balance between exercise sessions, while students in small groups are kept together.

%The division group into subgroup instead of student unit helps reduce the invalid combinations of students having different programs.

% In constraint programming, non-overlapped of student schedule requirement is modeled by soft different constraint. Therefore, the clustering students into subgroup can also decrease the number of constraints.

\subsubsection{Proposal of division of student groups}
Through the experiment, we suggest one way to divide large groups into smaller units. Overall, groups accounting for the largest percentage of each exercise session will be divided. The algorithm is presented in the upcoming section.

At the beginning, there is one subpart for  each group (line 3). \textit{NbSubgroups} is a map storing the number of subgroups of each group. For each course, the ratio of students in each group to the average number of students in an exercise session is computed (line 8). If the percentage is higher than 100 percent, the groups will be divided by 4 (line 9 -11). If the percentage is higher than 50 percent and less than 100 percent, then the group will be split into two subgroups (line 12 -14). 
The number of subgroups of each group is updated if its current value is smaller than the new one, \textit{nbSubPart} (line 15 -17). After running this algorithm, in each exercise session, there is no subgroup accounting for more 50\% in each exercise session. As a result, the arrangement of subgroups into different exercise sessions can help to reduce the deviation between them.


\begin{algorithm}
	\label{alg:subgroups}
	\caption{Calculate the number of subgroups when splitting large groups}
	\label{alg:cal_nbSubgroup}
	\begin{algorithmic}[1]
		\Procedure{Calculate\textendash Number\textendash Subgroup}{}
		\For{each group $g$ \Pisymbol{psy}{206} $Groups$ }
		\State	$NbSubgroups_{g} \gets 1$
		\EndFor
		\For{each course $c$ \Pisymbol{psy}{206} $Courses$} 
		\State	$EnrolledGroup \gets EnrolledGroupOfCourse(course)$
		\For{each group $g$ \Pisymbol{psy}{206} $EnrolledGroup$}
		\State $Percent_{g,course} \gets \dfrac  {NbStuPerGroup_{g} * 100}  {AvgStudentPerExercise_{course}}$
		\State $nbSubPart \gets 1$
		\If{$Percent_{g,course} \geq 100$} 
		\State  $nbSubPart \gets 4$
		\EndIf
		
		\If{$Percent_{g,course} \geq 50 \text{ AND } Percent_{g,course} \le 100 $} 
		\State  $nbSubPart \gets 2$
		\EndIf
		
		\If{$nbSubPart > NbSubgroups_{g}  $} 
		\State  $NbSubgroups_{g} \gets nbSubPart$
		\EndIf
		
		\EndFor
		\EndFor
		\EndProcedure
		
	\end{algorithmic}
\end{algorithm}


Notice that the decision of splitting a group depends solely on its percentage of its students on the average number of students in each exercise session. There is no limit set on the number of students of each group. For example, if group has more than 15 students then it needs to be divided into smaller units. The reason is that a group can  account for a minor part in one course, but it can be the major group in other courses


\begin{exmp}
	There are two courses, LMECA1901 and LELEC1530, in the program of the
	student group ME5el5. The average number of students per exercise session of  each course is 68 and 45 respectively. There are 49 students in the student group ME5el5 and it accounts for 72\% of the exercise session of LMECA1901 and 108\% for a session of LELEC1530. The group should be divided by 2 in LMECA1901 and by 4 in LELEC1530 course. As a result, the number of subgroups is the maximum of two values and the student group will be split by 4. 
		
	\begin{table}[!h]
		\centering
		\begin{tabular}{ccclllll|c|c|}
			\cline{1-3} \cline{9-10}
			\multicolumn{1}{|c|}{}          & \multicolumn{1}{c|}{Avg. students} & \multicolumn{1}{c|}{\begin{tabular}[c]{@{}c@{}}ME5el5 \\ (49 students)\end{tabular}} &  &  &            &  &  &           & \begin{tabular}[c]{@{}c@{}}ME5el5\\ (\# subGroups)\end{tabular} \\ \cline{1-3} \cline{9-10} 
			\multicolumn{1}{|c|}{LMECA1901} & \multicolumn{1}{c|}{68}            & \multicolumn{1}{c|}{72\%}                                                            &  &  & $\textcolor{red}{\implies}$ &  &  & LMECA1901 & 2                                                        \\ \cline{1-3} \cline{9-10} 
			\multicolumn{1}{|c|}{LELEC1530} & \multicolumn{1}{c|}{45}            & \multicolumn{1}{c|}{108\%}                                                           &  &  &            &  &  & LELEC1530 & 4                                                        \\ \cline{1-3} \cline{9-10} 
			\multicolumn{1}{l}{}            & \multicolumn{1}{l}{}               & 
		\end{tabular}
		\caption{Example of dividing a group into subgroups}
		\label{my-label}
	\end{table}
\end{exmp}
