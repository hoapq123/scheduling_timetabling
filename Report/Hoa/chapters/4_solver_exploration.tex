\section{Optimization techniques}
\subsection{Large Neighborhood Search}
As mentioned in chapter 2, Constraint Programming is a complete method, which means it exhaustively explores  the search space by using Branch and Bound Depth First Search. However, in hard Constraint Optimization problems, this method possibly experiences scalability issues. For example, a huge search tree cannot be explored entirely within a given duration and all the time is spent to explore a small region of the search space. Because of this, CP can be stuck in a small part of the search tree. This case is illustrated in the figure \ref{fig:cp_exploration}\footnote{The figure is inspired from the note of Constrain Programming course \cite{CP_course}}. 
\begin{figure}[!h]
	\centering
	\includegraphics[width=0.5\columnwidth]{images/cp_exploration.pdf}
	\caption{The search space of Constraint Programming. }
	\label{fig:cp_exploration}
\end{figure}

Large Neighborhood Search (LNS) is an incomplete method, which hybridizes Local Search (LS)  and Constraint Programming (CP) to solve large-scale COPs. Its idea is that when the search is stuck for a long time, it restarts and explores different regions of search search space. The process of LNS can be outlined in the following steps:

\begin{enumerate}
	\item Find the first solution and consider it as the current best solution. 
	\item Generate the neighborhood by relaxing a fragment of the variables to their initial domain. 
	\item Explore the neighborhood by CP with a search limit.
	\item If a solution is found, update it to the current best one. When the search limit is reached, restart and repeat step 2
\end{enumerate}

One advantage of LNS is that it tackles the scalability issue of CP and improves the diversification by exploring different neighborhoods. Moreover, it is quite easy to implement when the user is provided a relaxation procedure and a search limit \footnote{It is extracted from \cite{volns}}. 
\begin{itemize}
	\item Relaxation procedure defines the neighborhood around the current solution by relaxing a fragment of the variables. Other variables are fixed to their current value by adding temporary constraints to the problem.
	\item Search limit helps to avoid spending too much time on one region of search space. When the limit is reached, a restart is triggered and the search "jumps" to new a neighborhood. Its search space is similar to the figure \ref{fig:lns_exploration} \footnote{The figure is inspired from the note of Constrain Programming course \cite{CP_course}}.
\end{itemize}
\begin{figure}[!h]
	\centering
	\includegraphics[width=0.4\columnwidth]{images/lns_exploration.pdf}
	\caption{The search space of Large Neighborhood Search. }
	\label{fig:lns_exploration}
\end{figure}

Standard LNS only optimizes one objective function. Meanwhile, there are five objectives in the course timetabling problem of EPL. These objectives can be optimized together by introducing a weighted sum function including all of them. However, the sum constraint in CP leads to weak filtering on lower bounds (based on \cite{cp_sumConstraint}). Therefore, it was decided to optimize the objectives sequentially and their orders correspond to their priorities in the problem. In \cite{volns}, Pierre Schaus presents Variable Objective Large Neighborhood Search which allows to change the objective functions along restarts.

\subsection{Variable Objective Large Neighborhood Search (VO-LNS)}
Variable Objective Large Neighborhood Search is an extension of LNS which allows to optimize several objectives at the time and  to change the objective function dynamically along the iterations. It has two benefits \cite{volns}:
\begin{itemize}
	\item It provides a full control on the prioritization among the different
	objectives
	\item It allows stronger pruning during the branch and bound than using the weighted sum method.
\end{itemize}

The constrained optimization problem needs to minimize several objectives which can be included in a weighted sum function as follow:
\begin{equation*}
\begin{array}{l}
Optimize \quad z = obj_{1} + obj_{2} + ...  + obj_{m}\\
\textit{Subject to} \quad constraints
\end{array}	
\end{equation*}
To turn this optimization model into VO-LNS, the objectives can be stated as the following equation. 
\begin{equation*}
\begin{array}{l}
Optimize \quad obj = \{obj_{1},obj_{2} ... ,obj_{m}\} \\
\textit{Subject to} \quad constraints
\end{array}	
\end{equation*}
The difference is that several objectives are stated in VO-LNS model instead only one in the weighted function. For each iteration, each objective is assigned to one out of three different filtering modes which affect the pruning of the search space \cite{volns}:  
\begin{itemize}
	\item \textit{No-Filtering} deactivates the objective so it has no influence at all.
	\item \textit{Weak-Filtering}: If a solution is found the bound of the objective is updated. The value of the objective in the next solutions have to be better or equal to its current value.
	\item \textit{Strong-Filtering}: If a solution is found the bound of the objective is updated. The value of the objective in the next solutions have to be strictly greater than its current value.
\end{itemize}

Different configurations of the filtering modes can be set up to the
objectives of course timetabling problem when they are optimized successively. For example, when minimizing the first objective, it is assigned to \textit{Strong-Filtering} mode while the others are set to \textit{No-Filtering} (line 7 - 9). Once the value of the first objective reaches its minimum or the stop condition is met, the search moves to the next objective by changing the variable of the total conflicts in student schedule to \textit{Weak-Filtering} mode and set the second objective to \textit{Strong-Filtering} (line 15 - 17).  These configurations do not allow the degradation the value of the first objective during the search.

\lstinputlisting[numbers=left]{listings/4_volns.scala}