\section{Optimization Objectives}
Five objectives in the course timetabling of EPL are minimized sequentially. Through the experiment of optimizing all objectives,  \textit{dom/deg} variable heuristic was used. In this heuristic, the variable having the smallest ratio between its domain size and its number of constraints is be chosen first. The value assigned to a variable is selected randomly (except when optimizing the second objective). 

\subsection{Objective 1: Minimizing the total conflict in student schedule} \label{optimize_obj1}
The most important goal of course timetabling problem is to find a solution which minimizes the total conflict in student schedule. The schedule conflict of each subgroup is modeled by  \textit{softAllDifferent} in the 8th constraint \ref{cons8_conflict}. Based on this information, the total of conflict in the schedule of all students is calculated. %and it is the first optimized objective.

As mentioned in chapter 2, there are two approaches to solve constraint optimization problem. Both ways to optimize the first objective have been implemented and their results are presented in the next paragraphs.

\subsubsection{Branch and Bound} \label{sec:obj1_BB}
In this approach, the \textit{Oscar} solver is responsible for finding the optimality value of the  objective which was declared. Every time the solver finds a solution with value \textit{v}, it updates the constraint of objective variable to enforce the value of next solution strictly smaller than value \textit{v}. The final solution is considered as the best one.  

\textbf{Relaxation} 

Branch and Bound method is combined with Large Neighborhood Search (LNS) to improve the diversification of the search and avoid the heavy tail problem. In LNS exploration, a relaxation strategy needs to be implemented in order to define the neighborhood  when restarting a new search. Two different relaxation strategies has been implemented:
\begin{enumerate}
	\item Relaxing all variables of courses of each program in which its subgroups has the timetable clash
	\item Relaxing all variables of courses which overlap with the sessions of other courses having common students. 
\end{enumerate} 
In order to distinguish between two relaxation strategies, let us consider the following example:
\begin{exmp}
	Subgroup $EL4in4^{1}$ has one conflict in its schedule then the variables of courses in its program will be relaxed. 
	\begin{itemize}
		\item The first strategy relaxes all variables of lecture and exercise sessions of 6 courses in the program of group $EL4in4$ to their initial domain. (These courses are listed in example \ref{exp:program}). 
		\item The second strategy detects  courses which cause the conflict in the schedule. For example, the lecture sessions of two courses, \textit{LSINF1252} and \textit{LSINF1225}, occur at the same time, then only the variables of two courses will be relaxed
	\end{itemize}
\end{exmp}

Experiments with two relaxation strategies have been carried on and the results are shown in table \ref{tab:relax_strategy}. In these tests, the student groups are split into subgroups. 

\begin{table}[!h]
	\centering
	\begin{tabular}{|c|c|c|c|c|c|}
		\hline
		& \multicolumn{2}{c|}{Semester 5 - 2013} &  & \multicolumn{2}{c|}{Semester 4 and 6 - 2013} \\ \hline
		\#         & Best value        & Avg. Runtime       &  & Best value           & Avg. Runtime          \\ \hline
		Strategy 1 & 0                 & 30 s               &  & 649                  & 240 s                 \\ \hline
		Strategy 2 & 0                 & 22 s               &  & 583                  & 240 s                 \\ \hline
	\end{tabular}
	\caption{Comparison between two relaxation strategies.\\
		 	Experiment with data of semester 5 in 2013.}
	\label{tab:relax_strategy}
\end{table}
The result of the second relaxation strategy dominates that of the first strategy. The reason is that when the total number of conflicts is large, most subgroups have the timetable clash. According to the first strategy,  the variables of
many courses need to be relaxed because they belong to a program in which its subgroups have the schedule conflict. As a result, finding the next optimal solution in the large neighborhood consumes much more time than relaxing only courses having conflicts in the second strategy. 
%I also implement the second relaxation procedure which only focuses on courses which overlap with the sessions of other courses having common students. This approach reduces the number of relaxed courses, so the search space decreases. However, it takes time to compute the courses having no conflicts. The reason is that \textit{softAlldifferent} constraint records the cost of violation, but it does not detect which variables having the same value.  

\textbf{Splitting student groups into smaller units} 

There are two ways of splitting  a student group into smaller units: dividing them into individual students (atomic units) or subgroups (Algorithm \ref{alg:cal_nbSubgroup}). The following figure displays the result of the experiment implemented with data of semester 5 in 2013. It can be seen that the splitting student groups into subgroups obtains the optimal objective in 21 seconds. In contrast, the other approach could not yield the optimal result even after two minutes (Figure \ref{fig:BB_Q5}). The reason is that if a group is split into atomic units, %there are many invalid combinations of students having different programs in partial subscription exercise sessions. 
students having different programs can be mixed in the same exercise session. As a result,  there are many invalid combinations and the total conflicts in student schedule decreases slowly.
\begin{figure}[!h]
	\centering
	\includegraphics[width=0.8\columnwidth]{images/BBS5.pdf}
	\caption{Comparison between splitting group into subgroups and students. \\
		Experiment with data of semester 5 in 2013. }
	\label{fig:BB_Q5}
\end{figure}
The same experiment was done with data of semester 4 and 6 in 2013 and figure \ref{fig:BB_Q4Q6} illustrates its result. When the size of data increases, the branch and bound approach can not find the optimal objective in 4 minutes
\begin{figure}[!h]
	\centering
	\includegraphics[width=0.8\columnwidth]{images/BBS4S6.pdf}
	\caption{Comparison between splitting groups into individual students and subgroups. \\
		Experiment with data of semester 4 and 6 in 2013. }
	\label{fig:BB_Q4Q6}
\end{figure}

In summary, through these experiments, it can be seen that splitting groups into subgroups delivers better result and performance than splitting it into individuals. These results are suitable with the explanation and observation given in the section  \ref*{sec:subgroup}. Besides that, although this approach can find the optimal solution for semester 5 (S5), it was unable to identify the optimality when the size of the data increases (S4S6). 
  
\subsubsection{Iterative Optimization}
Practice revealed that the total conflicts in student schedule for each data instance is quite small. Otherwise, the solution of timetable  will not be accepted when it causes many clashes in the student schedule. Therefore,
an iterative approach was tested, starting from the lower bound of the objective and gradually increasing it. %try to find one solution.
For each search, a new constraint is added to enforce the total conflict is equal or less than the lower bound (line 7). Adding this constraint prunes the search space and removes parts not containing the optimal solution. If the search reaches the limit and solver has not found any solution, the lower bound increases by 1 (line 11 - 13). 

\lstinputlisting[numbers=left]{listings/4_Optimization_Iterative.scala}

The implementation was 100 times for each instance of data in 2013. On average, it took 0.8 s to find the first solution of semester 5 in 2013 (table \ref{table:iterative_S5}) , while data of semester 4 and 6 in 2013 needs 3.2 s (table \ref{table:iterative_S4S6}). From the result of the experiments, the total conflicts in student schedule is found faster by using iterative optimization method than by branch and bound approach. Even when the size of data increases, this method can solve the problem in reasonable time. However, in some tests, the iterative optimization approach can not find the best objective. For example, with the data of semester 5 in 2013, the optimal value is found only 56 times out of 100 runs. Meanwhile, the result of  the semester 4 and 6 in 2013 is worse, only 37 times out of 100. The reason is that even the search space is reduced by adding the constraint to restrict the value of objective, it is still too large for an exhaustive exploration. Therefore, the search can stop when reaching the limit number of backtracks.

\begin{table}[!h]
	\centering
	\begin{tabular}{|c|c|}
		\hline
		Found Value & \begin{tabular}[c]{@{}l@{}}Number times \\ (out of 100 tests)\end{tabular}       \\ \hline  \hline
		0                      & 56                 \\ \hline
		1                      & 26                 \\ \hline
		2                      & 8                  \\ \hline
		3                      & 5                  \\ \hline
		4                      & 1                  \\ \hline
		5                      & 3                  \\ \hline  \hline
		\multicolumn{2}{|c|}{The Optimal value : 0}     \\ \hline 
		\multicolumn{2}{|c|}{Average time: 0.8 s} \\ \hline
	\end{tabular}
	\caption{Result of optimization semester 5 in 2013 \\
		by using iterative method}
	\label{table:iterative_S5}
\end{table}

\begin{table}[!h]
	\centering
	\begin{tabular}{|c|c|}
		\hline
		Found Value & \begin{tabular}[c]{@{}l@{}}Number times \\ (out of 100 tests)\end{tabular}       \\ \hline  \hline
		54                     & 37                 \\ \hline
		55                     & 25                 \\ \hline
		56                     & 11                 \\ \hline
		57                     & 10                 \\ \hline
		58                     & 8                  \\ \hline
		59                     & 2                  \\ \hline
		60                     & 1                  \\ \hline
		62                     & 2                  \\ \hline
		65                     & 2                  \\ \hline
		71                     & 1                  \\ \hline
		72                     & 1                  \\ \hline \hline
		\multicolumn{2}{|c|}{The Optimal value : 54}    \\ \hline  
		\multicolumn{2}{|c|}{Average time: 3.2 s} \\ \hline
	\end{tabular}
	\caption{Result of optimization semester 4 and 6 in 2013 \\
		by using iterative method}
	\label{table:iterative_S4S6}
\end{table}


\subsubsection{Combining both approaches}
Through the analysis above, one sees that the advantage of iterative optimization is finding the solution in short time even with large-scale data. Meanwhile, branch and bound approach can find the optimal objective. Therefore, we suggest to use both methods to leverage their strong points. First, iterative optimization is used to find the first solution. The experiment was run 100 times with the data of 2013 and its average result is displayed in table \ref{tab:1step_iterative}. Clearly, the found value is quite close to the optimality. The average value of objective in semester 5 is 0.89 while it  is 56.05 in semester 4 and 6. Notice that, the runtime of first step includes the initialization of variables in the model.
\begin{table}[!h]
	\centering
	\begin{tabular}{|c|c|c|c|}
		\hline
		& Semester 5 &  & Semester 4 and 6 \\ \hline
		Runtime                 & 0.90 s      &  & 3.8 s          \\ \hline
		The optimal value       & 0          &  & 54               \\ \hline
		Avg. Value of objective & 0.89       &  & 56.05            \\ \hline
	\end{tabular}
	\caption{The average result of first solution found by iterative optimization. \\
	Testing with data of  2013.	}
		\label{tab:1step_iterative}
\end{table}

After having the first solution, the optimal objective is searched by branch and bound approach with Large Neighborhood search (LNS). 
The experiment was run with two relaxation strategies which are described in  Branch and Bound approach (section \ref{sec:obj1_BB}). 

\begin{enumerate}
	\item Relaxing all variables of courses of each program in which its subgroups has the timetable clash
	\item Relaxing all variables of courses which overlap with the sessions of other courses having common students. 
\end{enumerate} 


\begin{table}[!h]
	\centering
	\begin{tabular}{|c|c|c|c|c|c|}
		\hline
		& \multicolumn{2}{c|}{Semester 5 - 2013}            &  & \multicolumn{2}{c|}{Semester 4 and 6 - 2013}      \\ \hline
		\#         & Best value & Avg. Runtime step 2 &  & Best value & Avg. Runtime step 2 \\ \hline
		Strategy 1 & 0                    & 68 ms               &  & 54                   & 3250 ms             \\ \hline
		Strategy 2 & 0                    & 51 ms               &  & 54                   & 2490 ms             \\ \hline
	\end{tabular}
	\caption{Comparison branch and bound approach with \\ 
		two different exploration strategies}
\end{table}

Branch and bound approach can find the optimal objectives with both strategies (The total conflict is 0 in semester 5 and it is 54 with S4S6). However, the average of execution time of the second relaxation strategy is also shorter than the first one. Relaxing only courses which take the same slots with other courses having common students reduces the neighborhood and also gives the flexibility to re-optimize the current solution.  

% The following snippet code demonstrates the implementation of the second relaxation strategy during LNS search.\\
%\lstinputlisting[numbers=left]{listings/4_Optimization_combination.scala}

The search moves to the next objective iff  two conditions are met: either the current solution is equal to the lower bound of the objective variable or the duration from the last solution reaches the limit.  The found objective can be non-optimal if the stop condition falls into the second case. However, through the experiment with all instances of data (S5 in 2011 - 2013 and S4S6 in 2013) the best objective is always found.

\subsection{Objective 2: Minimizing the conflicts in non-preferred slots of professors}
\subsubsection{Generate non-preferred slots of each professor}
The second objective of the problem is to minimize the conflicts in non-preferred slots of professors. However, there is no information about the non-preferred  slots of each professor in the data of three years 2011, 2012 and 2013. Therefore, an instance generator was created to produce artificial data for testing purpose. The non-preferred slots of each professor are generated such that do not overlap his unavailable slots. The number of non-preferred  slots depends on the given percentage.

\begin{exmp}
	Professor A has 3 unavailable slots out of 20 slots. Thus, he can teach in 17 left slots. If the percentage of non-preferred slots is 30\%, then the generator will create 5 slots at which the professor prefers to avoid teaching (17 * 0.3 = 5.1).
\end{exmp} 

\subsubsection{Calculate the conflicts in non-preferred slots}
To compute the number of conflicts in non-preferred slots of each professor, each of his teaching session is checked whether it is in his unfavored  slots (line 5-6). After that, the maximum conflicts of a professor and the total conflicts of non preferred slots of all professors are computed.

\lstinputlisting[numbers=left]{listings/4_calculate_nonpreferedSlots_1session.scala}

The solution requires to ensure the fairness between professors so the total conflicts in unfavored slots should not compress on one person. Therefore, the following priority was set between objectives:
\begin{enumerate}
	\item The maximum number of conflicts in non preferred slots of a professor (subObjective1)
	\item The total conflicts in non preferred slots of all professors are computed (subObjective2)
\end{enumerate}
This requirement can be modeled by a weighted sum function. The formulation needs to ensure that the solution with smaller value of subObjective1 is considered better  than the other solution. Therefore, the value of subObjective1 is multiplied with the upper bound of the subObjective2.

\lstinputlisting[numbers=left]{listings/4_nonpreferedSlots_weightedsum.scala}

\subsubsection{Search}
Besides using the same variable heuristic like other parts, a specific value heuristic for this objective was devised. For each teaching session, an array  containing the number of conflicts in non-preferred slots is created. When choosing the value of the variable of each teaching session, a value which is in its domain and has the smallest number of conflicts is chosen first. If there are many values having the same number of conflicts, one of them is chosen randomly. %It is called greedy value heuristic.
\begin{exmp}
	The lecture session of course A has the array of number of conflicts in non-preferred slots like the table \ref{tab:conflict_non-prefer}. The size of the array is equal to the number of slots per week ($nbSlots$).
	This array is computed before the search beginning.
	
	The value of the fourth element in the array $NbConflict$ is 2. It means that if this lecture session is assigned to slot 4, it causes two conflicts in non-preferred slots of professors. Assume the current domain of variable of this lecture sessions has three values (1, 7, 4). Slots 1 and 7 have the lowest number of conflicts (1 conflict) and one  of them is chosen randomly. 
	\begin{table}[!h]
		\centering
		\begin{tabular}{llllllllllll}
			\cline{3-12}
			NbConflict & \multicolumn{1}{l|}{} & \multicolumn{1}{l|}{0} & \multicolumn{1}{l|}{1} & \multicolumn{1}{l|}{2} & \multicolumn{1}{l|}{0} & \multicolumn{1}{l|}{2} & \multicolumn{1}{l|}{0} & \multicolumn{1}{l|}{1} & \multicolumn{1}{l|}{1} & \multicolumn{1}{l|}{...} & \multicolumn{1}{l|}{0} \\ \cline{3-12} 
			&                       &                        &                        &                        &                        &                        &                        &                        &                        &                        &                        \\
			Index &                       & 0                      &  \textcolor{red}{1}                      & 2                      & 3                      & \textcolor{red}{4}                      & 5                      & 6                      & \textcolor{red}{7}                      & ...                      & 19                     
		\end{tabular}
		\caption{An example of the array of number of conflicts in non-preferred slots of a lecture session. \\ Based on this array, a value is chosen for its variable.}
	\end{table}
	\label{tab:conflict_non-prefer}
\end{exmp}

\begin{table}[!h]
	\centering
	
	\begin{tabular}{|c|c|c|c|c|c|c|c|c|}
		\hline
		& \multicolumn{2}{c|}{Random value heuristic} &  & \multicolumn{2}{c|}{Specific value heuristic} &  & \multicolumn{2}{c|}{Optimal Result} \\ \hline
		Percentage & Max Con.      & Objective2     &  & Max Con.          & Objective2         &  & Max Con.         & Objective2        \\ \hline
		10         & 1                 & 40                   &  & 1                    & 40            &  & 1                    & 40           \\ \hline
		20         & 0                 & 0                    &  & 0                    & 0             &  & 0                    & 0            \\ \hline
		30         & 1                 & 40                   &  & 1                    & 40            &  & 1                    & 40           \\ \hline
		40         & 2                 & 87.1                 &  & 2                    & 83            &  & 2                    & 83           \\ \hline
		50         & 2                 & 84                   &  & 2                    & 84            &  & 2                    & 84           \\ \hline
		60         & 1                 & 45                   &  & 1                    & 45            &  & 1                    & 45           \\ \hline
		70         & 2                 & 93.6                 &  & 2                    & 91.6          &  & 2                    & 91           \\ \hline
		80         & 2                 & 99.7                 &  & 2                    & 96.2          &  & 2                    & 96           \\ \hline
		90         & 2                 & 103.4                &  & 2                    & 100           &  & 2                    & 100          \\ \hline
		100        & 3                 & 156                  &  & 3                    & 156           &  & 3                    & 156          \\ \hline
	\end{tabular}
	\caption{Comparing the results of two heuristics with the optimal value.\\ Testing with data of semester 5 in 2013}
	\label{tab:non-preferred-heuristic}
\end{table}

With each given percentage, the experiment is run 10 times and its average result is illustrated in  table \ref{tab:non-preferred-heuristic}. With both heuristics, the maximum number of conflicts of a professor obtains the optimal result. The score of $objective2$ is better with the specific value heuristic than with the random, especially when the percentage of non-preferred slots increases. However, in some tests, the optimal objective is not found %even with the specific value heuristic. 
The reason is that the search moves to the next objective if the duration from the last solution exceeds the time limit.

\subsubsection{Relaxation}
If a lecture session or exercise session occurs in non-preferred slots of their responsible professors, then the variable of this session is relaxed. Meanwhile, a portion of teaching sessions having no conflict in non-preferred slots is fixed. %Notice that if the exercise of a course is fixed then the position of each subgroup in its exercise sessions is also fixed.

\subsection{Objective 3: Maximizing the quality of schedule}
\subsubsection{Definition of the third objective}

 In the equation computing the quality of schedule (equation \ref{equ:objective3}), the denominator is constant. Therefore, the maximization the quality of schedule is reduced to the maximization of the weighted function $(valueLecture * 2 + valueExercise)$. It is presented in dual way by minimizing the negative value of the weighted function. Therefore, the third objective function is defined like the following code snippet:

\lstinputlisting[numbers=left]{listings/4_Objective3.scala}

%The quality of schedule is calculated easily by dividing the value of objective 3 to the expression $totStudentAllLecture * 2 + totStudentAllExercise$. When optimizing objective 3,  the variable heuristic is dom/deg and the value of each variable is chose randomly.

\subsubsection{Relaxation}
If a teaching session is scheduled in the low quality slots (smaller than one), then its variable is relaxed. Meanwhile, a portion of lecture and exercise sessions occurring in the middle of day will be fixed to the  schedule of the current solution. 

\subsubsection{Result}
The experiments with the data of semester 5 from 2011 to 2013 were run and their results are displayed in figure \ref{fig:qualitySchedule_Q5}. The quality of schedule in 2012 and 2013 takes a long time to improve and their results are 81.6\% in 2012 and 82.9\% in 2013. In contrast, the optimization of \textit{objective2} with the data of semester 5 in 2011 takes a shorter time (about 20s) to reach the best value (97\%). 
\begin{figure}[!h]
	\centering
	\includegraphics[width=0.8\columnwidth]{images/QualitySchedule.pdf}
	\caption{The quality of schedule in  semester 5 from 2011 to 2013. }
	\label{fig:qualitySchedule_Q5}
\end{figure}

\subsection{Objective 4: Minimizing the total number of students attending the end slot of day}

\subsubsection{Definition of the fourth objective}
As mentioned in the third chapter, the minimization of the total number of students attending the end slot of day is equivalent to the minimization of the expression $(valueLectureEndSlots * 2 + valueExerciseEndSlots)$. The following code snippet demonstrates the function of the fourth objective.

\lstinputlisting[numbers=left]{listings/4_Objective4.scala}

From the equation \ref{equ:endSlots}, the quality of the schedule in the end slots is calculated easily by dividing the value of $objective4$ by the value of the expression $totStudentAllLecture * 2 + totStudentAllExercise$. 

\subsubsection{Relaxation}
If a teaching session is scheduled in the end slot of day, then its variable is relaxed. Meanwhile, a portion of lecture and exercise sessions not occurring in the end slot of day will be fixed to the schedule of the current solution.

\subsubsection{The reason to introduce the fourth objective} \label{sec:reason_obj4}
Clearly, the end slot of day has the lowest value in the \textit{qualitySlots} array so the minimization of the total number of students attending the end slot of day leads to the increase in the quality of schedule. It can be seen that the fourth objective has the same goal of improving the quality of schedule.
An interesting question is that why  an extra objective is introduced while it is included in the third one. The first reason is that by using LNS, users have no proof of optimality for the found value of the objective because the search stops when the search limit is reached. For example, the third objective moves to the next goal when the duration from the last solution to the current exceeds the given time. In addition, when focusing on the end slot of day, only variables of teaching sessions occurring in the last time of day will be relaxed. As a result, this relaxation strategy defines a smaller neighborhood in comparison with the strategy when maximizing the quality of schedule. It is expected to find a better solution when increasing the intensification of the search. In fact, the quality of schedule still increases when minimizing of the total number of students in the end slots. The results are demonstrated in the table \ref{tab:obj4}

\begin{table}[!h]
	\centering
	\begin{tabular}{|c|c|c|c|c|c|}
		\hline
		& \multicolumn{2}{c|}{Result of optimizing objective 3} &  & \multicolumn{2}{c|}{Result of optimizing objective 4} \\ \hline
		Year & Quality of schedule       & Quality of end slots      &  & Quality of schedule              & Quality of end slots               \\ \hline
		2011 & 96.7\%                    & 1.62\%                    &  & 97.04\%                  & 1.53\%                     \\ \hline
		2012 & 82.2\%                    & 19.6\%                    &  & 82.28\%                   & 19.33\%                     \\ \hline
		2013 & 87.6 \%                   & 14.8 \%                   &  & 88.7\%                   & 12.9\%                     \\ \hline
	\end{tabular}
	\caption{The improvement of the quality of schedule after optimizing objective 4}
	\label{tab:obj4}
\end{table}

It can be seen that the quality of schedule is improved after optimizing the fourth objective in two years 2011 and 2013, while it does not change much in 2012. 

\subsection{Objective 5: Minimizing the total deviation of number of students in  exercise sessions}
One of requirements of the problem is to arrange students in exercise sessions equally. However, this requirement is often too strong for realistic instances so  it is relaxed. %The ratio of students attending in each exercise session to the average of number of students in this session is at least equal to  \textit{RelaxMargin} parameter. 
Therefore, the last objective is to minimize the total deviation of number of students in exercise sessions. 

The number of students in exercise sessions is restricted by the \textit{binPacking} constraint (In section \ref{cons9_balance}). In this constraint, the load of each bin is also the number of students in each exercise session. From this, the deviation of number of students in each session with the average number of students is computed (line 5-6). The total deviation is the sum of the deviation in all exercise sessions.

\lstinputlisting[numbers=left]{listings/4_obj5_deviation.scala}

\subsubsection{Relaxation}
The schedule of lecture and exercise sessions is fixed to the current solution.
Meanwhile, if the ratio of the deviation of real number of students attending each session to the average number of students of this session is greater than \textit{Margin} percent, then the variables of index of subgroups in this exercise are relaxed. 

To recall, the ratio of the deviation of number of students attending each session to the average number of students of this session is computed as the following equation.
\begin{equation}
\begin{array}{l}
\forall course \in PartialSubscriptionExercise \\
\quad \forall session \in ExerciseSession_{course}:\\
\qquad ratioDeviationStudentInSession_{session} = \dfrac{NbStudentInExerciseSession_{session}}{AvgStudentPerExercise_{course} }
\end{array}
\end{equation}

\subsubsection{Result}
Table \ref{tab:obj5_deviation} illustrates the results of the experiments with data of semester 5 in three years 2011 to 2013. 
In the experiments, \textit{Margin} parameter is set to 0.8 and  \textit{RelaxMargin} is set to a smaller value, 0.7. 
In general, the percentage of deviation of number students in exercise sessions (computed by equation \ref{equ:ratioDeviation})  is smaller than 20\%. 

\begin{table}[!h]
	\centering
	
	\begin{tabular}{|c|c|c|}
		\hline
		Year & The total deviation & Percentage of deviation \\ \hline
		2011 & 49                  & 9\%                     \\ \hline
		2012 & 294                 & 15\%                    \\ \hline
		2013 & 91                  & 10\%                     \\ \hline
	\end{tabular}
	\caption{The total deviation of number of students in exercise sessions}
	\label{tab:obj5_deviation}
\end{table}
