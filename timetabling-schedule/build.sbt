lazy val root = (project in file(".")).
  settings(
    name := "timetabling-schedule",
    scalaVersion := "2.11.4",
    resolvers += "Oscar Snapshots" at "http://artifactory.info.ucl.ac.be/artifactory/libs-release/",
    libraryDependencies += "oscar" %% "oscar-cp" % "3.1.0" withSources()
  )
