package launcher

import solver.ProblemData

import scala.io.Source
import scala.collection.mutable.Map
//import scala.collection.mutable.Set
/*
 * Parser input file as its arguments and called by Launcher
 */
class TTSScaffolder(nbDay: Int, nbSlotPerDay:Int,maxNbActivities:Int, marginStudent: Float 
    ,strSemester:String,pathDirectory:String)  extends ProblemData{
  
    this.nDay = nbDay
    this.nSlotPerDay = nbSlotPerDay
    this.maxActivities = maxNbActivities
    this.marginStudentInExercise = marginStudent
    this.semester = strSemester
    
    parsingFiles
    
    def parsingFiles(){
        parsingProgram
        parsingCourse
        prasingProfSlot
        prasingGroup
        parsingProfCourse
    }
    def parsingProfCourse(){
        val courseFile = pathDirectory  + "/profCourse.txt"
        val lines = Source.fromFile(courseFile).getLines().filter(!_.trim.isEmpty).toList
        for(line <- lines){
            val parts = line.trim.split(":")
            val profs = if (parts.size >= 3) parts(2).split(" ").toSet else Set[String]()
            if(parts(0) == "course"){
                this.courseGivenByProfessor += parts(1) -> profs
            }else if(parts(0) == "exercise"){
                this.exerciseHandledByProfessor += parts(1) -> profs
            }
        }
        
        // Mapping list course responsible by a prof
        for((c,setProf) <- courseGivenByProfessor){
            for(p <- setProf){
                val setCourses = professorTeachingCourse.getOrElse(p, Set())
                val newSetCourses = setCourses + c
                professorTeachingCourse += p -> newSetCourses
            }
        }
    }
    
    def parsingProgram(){
        val courseFile = pathDirectory  + "/program.txt"
        val lines = Source.fromFile(courseFile).getLines().filter(!_.trim.isEmpty).toList
        for(line <- lines){
            val parts = line.trim.split(":")
            val program = parts(0)
            val courses= parts(1).split(" ").toSet
            this.program += program -> courses.toSet
        }
        
        // initialize the default parameters of all courses
        this.courses = this.program.values.flatten.toSet
        // each course has default 1 lecture session 
        this.courses.map(c => this.nbLecture += c -> 1)
        // each course has default 2 exercise session and 1 is mandatory 
        this.courses.map(c => this.nbExerciseOfCourse += c -> (1,2))
        
        // Mapping list program to course
        for((p,courses) <- this.program){
            for(c <- courses){
                val setProgram = this.courseInProgam.getOrElse(c, Set())
                val newSetProgram = setProgram + p
                this.courseInProgam += c -> newSetProgram
            }
        }
    }
    
    def parsingCourse(){
        val courseFile = pathDirectory  + "/course.txt"
        val lines = Source.fromFile(courseFile).getLines().filter(!_.trim.isEmpty).toList
        
        for(line <- lines){
            val parts = line.trim.split(":")
            if(parts(0) == "NbLecture"){
                this.nbLecture.update(parts(1), parts(2).toInt)
            }else if(parts(0) == "NbExercise"){
                val nbSessions = parts(2).split(" ")
                val (nbSubcription,nbExercise) = (nbSessions(0).toInt,nbSessions(1).toInt)
                this.nbExerciseOfCourse.update(parts(1),(nbSubcription,nbExercise))  
            }else if(parts(0) == "FixedLecture"){
                val slots = parts(2).split(" ").filter(!_.isEmpty()).map(_.toInt)
                this.fixedLectureOfCourse += parts(1) -> slots
            }else if(parts(0) == "FixedExercise"){
                val slots = parts(2).split(" ").filter(!_.isEmpty()).map(_.toInt)
                this.fixedExerciseOfCourse += parts(1) -> slots
            }else if(parts(0) == "ContinousCourse"){
                val courses = parts(1).split(" ").toSet
                this.continuousCourses = courses
            }
        }
        this.updateCourseInfo
    }
    
    def prasingGroup(){
        val courseFile = pathDirectory  + "/group.txt"
        val lines = Source.fromFile(courseFile).getLines().filter(!_.trim.isEmpty).toList
        for(line <- lines){
            val parts = line.trim.split(":")
            this.nbStudentPerGroup += parts(0) -> parts(1).toFloat
        }
    }
    
    def prasingProfSlot(){
        val courseFile = pathDirectory  + "/profSlot.txt"
        val lines = Source.fromFile(courseFile).getLines().filter(!_.trim.isEmpty).toList
        for(line <- lines){
            val parts = line.trim.split(":")
            if(parts(0) == "unavailable"){
                val slots = parts(2).split(" ").filter(!_.trim.isEmpty).map(_.toInt)
                this.unavailableTimeOfProf += parts(1) -> slots  
            }else  if(parts(0) == "lowtime"){
                val slots = parts(2).split(" ").filter(!_.trim.isEmpty).map(_.toInt)
                this.lowTimeOfProf += parts(1) -> slots 
            }
            
        }
    }
    
}