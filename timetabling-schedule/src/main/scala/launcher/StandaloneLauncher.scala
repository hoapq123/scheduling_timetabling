package launcher

import solver._

object StandaloneLauncherLauncher extends App {
    if (args.size < 1) {
        println("Usage : java -jar pathDirectory [semester] [numberDay] [numberSlot] [runTime] [pcNonPrefferedSlot]")
        System.exit(-1)
    }
    
    // Parameters
    var semester = "Q5"
    var nbDay =  5
    var nSlotPerDay = 4
    var maxActivities = 50
    var marginStudentInExercise = 0.8f 
    var path = "data/2013/Q5" 
    var runTime = 300000                          // 5 minutes do the search
    var pcNonPrefferedSlot = 0.2f
    // Read input options
    var list = args.toList
    do{
        list = nextOption(list)
    }while(!list.isEmpty)
    
    val problemData = new TTSScaffolder(nbDay,nSlotPerDay,maxActivities,
                                            marginStudentInExercise, semester, path)
    
    println("path: " + path + ", semester: " + semester)
    
    val startTime = java.lang.System.currentTimeMillis()

    searchStart()
    
    def searchStart(){
      println("pcNonPrefferedSlot " + pcNonPrefferedSlot)
      val model = new TTSModel(problemData,100,pcNonPrefferedSlot)  
                                with GenerateNonPreferedSlot
                                with TTSRelaxation with DomDeg_Heuristic 
                                with PrintSolution
      model.objectiveProgress(startTime)      
      model.statisticData()                                    
      model.searchBySubGroup(startTime,runTime)
      model.printSolution
    }
    val lastTime = java.lang.System.currentTimeMillis()
    println( "Time: \t" + (lastTime - startTime))
    
    def nextOption(list: List[String]) = {
        def isSwitch(s : String) = (s(0) == '-')
        list match {
          case Nil => List()
          case "--nbDay" :: value :: tail => {
              nbDay = value.toInt
              tail
          }
          case "--nbSlot" :: value :: tail =>{
              nSlotPerDay = value.toInt
              tail
          }
          case "--maxAct" :: value :: tail =>{
              maxActivities = value.toInt
              tail
          }
          case "--margin" :: value :: tail =>{
              marginStudentInExercise = value.toFloat
              tail
          }
          case "--semester" :: value :: tail => {
              semester = value.toString()
              tail
          }
          case "--runTime" :: value :: tail => {
              runTime = value.toInt
              tail
          } 
          case "--pcNP" :: value :: tail => {
              pcNonPrefferedSlot = value.toFloat
              tail
          }
          case string :: tail  => {
              path = string
              tail
          }
          case _ => List()
        }
    }
}