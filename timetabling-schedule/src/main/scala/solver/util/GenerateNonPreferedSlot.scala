package solver

import scala.collection.mutable._
import java.io._

/*
 * Generate randomly low time of each professor with a constant percentage
 */
trait GenerateNonPreferedSlot extends TTSModel{
  
    randomGeneratNonPreferedSlot
    /* 
     * Artificially generate random non preferred slots
     * Input : 
     * 		pcLowTime : percentage of non preferred slots over available slots
     */ 
    def randomGeneratNonPreferedSlot(){  
        if (pcNonPreferredSlot == 0)
          return
        for(prof <- professors){
            val unAvaiSlots = data.unavailableTimeOfProf.getOrElse(prof, Array())
            val availableSlots = rSlots.diff(unAvaiSlots)
            val lowTimeSlots = ArrayBuffer[Int]()
            val nbAdded = (availableSlots.size * pcNonPreferredSlot).round.toInt
            val rand = scala.util.Random
            
            for(i <- 0 until nbAdded){
                val addedPossible = availableSlots.diff(lowTimeSlots)
                val randIndex = rand.nextInt(addedPossible.size)
                lowTimeSlots.append(addedPossible(randIndex))
            }
            data.lowTimeOfProf.update(prof, lowTimeSlots.toArray)
        }
        
        // check generation of lowtime
        for((prof,lowTimeSlots) <- data.lowTimeOfProf){
            val intersect = lowTimeSlots.intersect(data.unavailableTimeOfProf.getOrElse(prof, Array()))
            if(intersect.size > 0)
              println(prof + ": intersect " + intersect.mkString(","))
        }
        
    }
    
    def writeProfLowTimeForCP(pc:Int){
      val dirPath = "nonPreferredSlot/" + data.semester 
      //1. creating director 
      val theDir = new File(dirPath);
      if (!theDir.exists()) {
          theDir.mkdirs()
          println("create directory" + dirPath)
      }
        
      var outFile = dirPath + "/" + data.semester + "_" + pc + ".scala"
      println(outFile)
      val file = new File(outFile )
      val bw = new BufferedWriter(new FileWriter(file))
      var str  = "package checking.fixNonPreferredSlots.Y2013 \n" + 
                  "import solver.TTSModel \n" +
                            "trait Q5_" + pc + " extends TTSModel{"
       bw.write(str)
       bw.append(System.lineSeparator)                     

       for((prof,lowTimeSlots) <- data.lowTimeOfProf){
            str = "\tdata.lowTimeOfProf.update(\"" + prof + "\", Array(" + lowTimeSlots.mkString(",") +  "))"
            bw.write(str)
            bw.append(System.lineSeparator) 
        }
       bw.write("}")
       bw.append(System.lineSeparator) 
      bw.close()
    }
    def writeProfLowTime2Mip(pc:Int){
      val dirPath = "nonPreferredSlot/" + data.semester 
        //1. creating director 
        val theDir = new File(dirPath);
        if (!theDir.exists()) {
            theDir.mkdirs()
            println("create directory" + dirPath)
        }
        
      var outFile = dirPath + "/" + data.semester + "_" + pc + ".dat"
      println(outFile)
      val file = new File(outFile )
      val bw = new BufferedWriter(new FileWriter(file))
      
      val slotsString = Array("L12","L34","L56","L78",
                          "M12","M34","M56","M78",
                          "W12","W34","W56","W78", 
                          "J12","J34","J56","J78",
                          "V12","V34","V56","V78")
        for((prof,lowTimeSlots) <- data.lowTimeOfProf){
            var str = "set Indispf[" + prof + "] := "
            lowTimeSlots.foreach{s => str += "(" + slotsString(s) + ",*) Qa Qb "}
            str += ";"
            bw.write(str)
            bw.append(System.lineSeparator)
        }
      bw.close()
    }
}