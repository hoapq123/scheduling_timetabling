package solver

import oscar.cp._
import oscar.util._
import scala.collection.mutable.Map
import scala.collection.mutable.ArrayBuffer

import oscar.cp.core.variables.CPIntVarViewMinus
import oscar.cp.core.variables.CPIntVarViewOffset
import scala.math.min
/*
 * Divide the most largest groups of each course into smaller subgroups
 */
trait PartitionToSubGroup extends TTSModel{
    /* groups which account for more than limited percent in each course will be divided*/
    val limitedPercent = 40
    // minimum percentage of student over the average in each exercise sessions
    val relaxMagin = 0.7 
    
    val lstGroupObj = Map[String,GroupInfo]()
    
    splitGroupIntoSubGroup()
    
    /* Initialize object for each group */
    for((group,nbStu) <- nbStudent){
        /*
         * Create an objective per group which contains all information and variables of subgroups
         */
        val groupInfo = new GroupInfo(group,nbStu,nbSubGroups(group))
        lstGroupObj += group -> groupInfo
    }
    
    /* Initialize the conflict schedule variable of each subgroup  */
    for(group <- groupStudent; i <- 0 until nbSubGroups(group) ){
        val nbSessionOfSubgroup = nbSessionOfGroup(group)

        val violation  = CPIntVar(0 until nbSessionOfSubgroup)
        subGroupViolation += (group,i) -> violation
        // number student for each subgroup
        nbStudentSubgroup += (group,i) -> lstGroupObj(group).nbStudentPerSubGroup(i)
    }
    /*
     * Map subgroup exercise variables to its corresponding courses
     */
    for((course,groups) <- data.courseInProgam; if partialSubscriptionExercise.contains(course)){
        val varSubGroupExercise = ArrayBuffer[CPIntVar]()
        for(group <- groups){
            val groupObj = lstGroupObj(group)
            varSubGroupExercise ++= groupObj.uniExerciseByCourse(course)
        }
        subgroupExerciseVariable += course -> varSubGroupExercise.toArray
    }
    
    /* 
     * SubGroup Schedule 
     * 		includes partial subscription exercise and full subscription 
     */
    for(group <- groupStudent;i <- 0 until nbSubGroups(group) ){
        val obj = lstGroupObj(group)
        
        val oneSubGroupSchedule = ArrayBuffer[CPIntVar]()
        for((course,varIndex) <- obj.subGroupUniExercise(i)){
            oneSubGroupSchedule += slotExercise(course)(varIndex)
        }
        oneSubGroupSchedule ++= groupSchedule(group) 
        
        subGroupSchedule += (group,i) -> oneSubGroupSchedule
    }
    /*
     * C7: Calculate the conflict schedule of each subgroup 
     */
    //TODO : update Oscar to new version, change to softAlldiferent
    for(group <- groupStudent; i <- 0 until nbSubGroups(group)){
        val nVariables = subGroupSchedule(group,i).length
        val nValues = new CPIntVarViewOffset(new CPIntVarViewMinus(subGroupViolation(group,i)), nVariables)
        add(atLeastNValue(subGroupSchedule(group,i), nValues), Strong)
    }
       
    /*
     * C2: Balancing between exercise session 
     */
    val nbStuInExerciseSessionVar = Map[String,Array[CPIntVar]]()
    for(course <- partialSubscriptionExercise){
        val nbExercise = data.nbExercise(course)
        val groups = data.courseInProgam(course)
        val minStudentPerSlot = (avgStuPerExerciseSession(course) * relaxMagin).toInt          
        val maxStudentPerSlot = totStuExerciseSession(course) - minStudentPerSlot * (nbExercise - 1)
        
        val loads = Array.fill(nbExercise)(CPIntVar(minStudentPerSlot to maxStudentPerSlot))
        
        val varOfExerciseSubGroup = ArrayBuffer[CPIntVar]()
        val nbStuInSubGroup = ArrayBuffer[Int]()
        
        for(group <- data.courseInProgam(course); if nbStudent(group) > 0){
            varOfExerciseSubGroup ++= lstGroupObj(group).uniExerciseByCourse(course)
            nbStuInSubGroup ++= lstGroupObj(group).nbStudentPerSubGroup
        }
        
        add(binPacking(varOfExerciseSubGroup.toIndexedSeq, nbStuInSubGroup.toArray, loads))
        
        nbStuInExerciseSessionVar += course -> loads
        
        /*
         * courses which have more than 1 sub group to attend
         */
        val maxOccurrence = varOfExerciseSubGroup.size - nbExercise + 1
        add(gcc(varOfExerciseSubGroup,0 until nbExercise,1,maxOccurrence))
    }
    
    cp.addDecisionVariables(nbStuInExerciseSessionVar.values.flatten)
    
    /*
     * Calculate number subgroup of each group
     * Make changes of map nbSubGroups
     */
    def splitGroupIntoSubGroup(){
      // Initial number subgroups in each group. Default is 1 subgroup
      for((group,nbStu) <- nbStudent){
          nbSubGroups += group -> 1
      }
      /* For each course, divide the largest groups */
      for(course <- partialSubscriptionExercise){
          val groups = data.courseInProgam(course)
          val sortedGroups = groups.toList.sortBy(g => -nbStudent(g))
          var isStop = false
          var i = 0
              
          /* calculate number subgroups for each group in this course */
          do{
              val group = sortedGroups(i)
              val percent = (nbStudent(group) * 100/avgStuPerExerciseSession(course))
              var nbSub = 2
              if(percent >= limitedPercent){
                  if(percent > 2 * limitedPercent) nbSub = 4
                  if(nbSub > nbSubGroups(group)) nbSubGroups.update(group, nbSub)
              }else {
                  isStop = true
              }
              
              i += 1
              
          }while(!isStop && i < groups.size)
      }
    }

    def statisticSubGroup(){
      for((course,groups) <- data.courseInProgam; if partialSubscriptionExercise.contains(course)){
        val sortedGroups = groups.toList.sortBy(g => -nbStudent(g))
        println("course: " + course + ": " + totStuExerciseSession(course) + " total,nb Session " 
                          + data.nbExercise(course) + " "+ avgStuPerExerciseSession(course) + " avg")
        
        for(i <- 0 until groups.size){
            val group = sortedGroups(i)
            val percent = (nbStudent(group) * 100/avgStuPerExerciseSession(course))
            println("\t" + group + " has " + nbStudent(group) + " students, account for " + percent 
                                                      + " divide to " +  lstGroupObj(group).nbSubGroup)
        }
      }
    }
    /*
     * Contains uni exercises variable of all subgroups in a group
     */
    class GroupInfo(val groupName: String,val totStudent: Int, val nbSubGroup : Int ){
        /* Initialize number student in each subgroup */
        val nbStudentPerSubGroup =  Array.fill(nbSubGroup)(totStudent/nbSubGroup)  
        
        /* Uni exercise variable of each group */
        val subGroupUniExercise = Array.fill(nbSubGroup)(Map[String,CPIntVar]())
        /* Exercise variables group by their courses */
        val uniExerciseByCourse = Map[String,Array[CPIntVar]]()
        
        initilizeUniExerciseVariable
                
        def initilizeUniExerciseVariable(){
            // update the last subgroup
            if(totStudent % nbSubGroup > 0){
                nbStudentPerSubGroup(nbSubGroup - 1) = totStudent/nbSubGroup + totStudent % nbSubGroup 
            }
            
            for(course <- data.program(groupName); if partialSubscriptionExercise.contains(course)){
                 
                 val arrValOfCourse  = ArrayBuffer[CPIntVar]()    // add uniexercise variable of each group by course
                 // create group uniexercise variable  
                 for(i <- 0 until nbSubGroup ){
                     val slotSubGroup = CPIntVar(0 until data.nbExercise(course),name = course + ":sub:" + groupName + ":" + i)
                     // add to each subgroup schedule
                     subGroupUniExercise(i) += course -> slotSubGroup 
                     // add exercise variable to its course
                     
                     arrValOfCourse += slotSubGroup
                 }
                 // add all exercise variables to their course
                 uniExerciseByCourse +=  course -> arrValOfCourse.toArray
            }
        }
    }
}