package solver

import oscar.cp._
import oscar.util._
import scala.collection.mutable.Map
import scala.collection.mutable.ArrayBuffer
/**
 * @author CongHoa
 */
trait SearchLauncher extends TTSOptimization{
      /**********************************************/
      /*              Search                        */
      /**********************************************/
      var bestLectureSolution = Map[String,Array[Int]]()
      var bestExerciseSolution = Map[String,Array[Int]]()
      var bestGroupUniExercise = Map[String, Map[String,Int]]()
      var nbRestart = 0
      decisionVariables ++= subgroupExerciseVariable.values.flatten
      cp.addDecisionVariables(decisionVariables)
              
      /* Store timetable schedule of lecture and exercises */
      onSolution{
          for((course,sessionVar) <- slotLecture){
              bestLectureSolution += course -> sessionVar.map(x => x.value) 
          }
          
          for((course,sessionVar) <- slotExercise){
              bestExerciseSolution += course  -> sessionVar.map(x => x.value) 
          }
      }
      
      /************************************************************************/
      /*    Value heuristic for optimizing conflicts in non-preferred slots		*/
      /************************************************************************/
      val isGreedy_nonPreferredSlot = true
      val variableWithConflictOfSlot = Map[CPIntVar,Array[Int]]()
      if (isGreedy_nonPreferredSlot)
          nbConflictOfProf_OfValueOfSession
      
      /*
       * Optimizing : non-preferred conflicts of professor.
       * Map teaching session variables with the number conflicts in non-preferred  slots with each value   
       */
      def nbConflictOfProf_OfValueOfSession(){
          /* Calculate the number conflicts of each course with different each value slot  */
          val conflictOfCourseForSlot = Map[String,Array[Int]]()
          
          for((course,profs) <- data.courseGivenByProfessor){
              val conflictOfCourse = Array.fill(nSlots)(0)  
              for(p <- profs; s <- data.lowTimeOfProf.getOrElse(p,Array()))
                  conflictOfCourse(s) += 1
              /* check whether course has slots in lowtime then need greedy value algorithm */    
              if(conflictOfCourse.max > 0)    
                conflictOfCourseForSlot += course -> conflictOfCourse
          }
          
          // Map variable of teaching unit with the correspond conflicts of each slot  
          for((course,arr) <- conflictOfCourseForSlot)
            slotLecture(course).map{ x => variableWithConflictOfSlot += x -> arr }
                  
          }
      
      /* 
       * For search greedy value heuristic for 
       * optimizing conflict in non-preferred slots of professor 
       */
      def valueHeuristic_ConflictOfProf(x:CPIntVar): Int = {
          /*
           *  if variable is lecture or exercice session 
           *  which can cause the conflict in non-preferred slots of professor.
           */
          if (variableWithConflictOfSlot.contains(x) ){
            // array conflict of prof for each value of the session variable
            val conflictOfSlots = variableWithConflictOfSlot(x)
            // find the slot which has the minimum conflict in non-preferred of professor
            val minConflictSlot = x.minBy { v => (conflictOfSlots(v),rand.nextInt(nSlots)) }
            return minConflictSlot
          }else{
            val rand = scala.util.Random
            return x.randomValue(rand)
          }
      }
}

trait Custom_Heuristic extends SearchLauncher{
    
    /*
     * Value heuristic for subgroupExerciseVariable.
     * 		Choose variables in :  - course having small number of students 
     * 													 - subgroup in this course having large number of students
     */
    val mapSubGroupPriority = Map[CPIntVar,Int]()
    for((group,groupObj) <- lstGroupObj){
        for(i <- 0 until groupObj.nbSubGroup; (course,x) <- groupObj.subGroupUniExercise(i)){
            val score = avgStuPerExerciseSession(course) * 2 - groupObj.nbStudentPerSubGroup(i)
            mapSubGroupPriority += x -> score
        }
    }
    /* Assign variables with their priority in the search process
       * 	 	- lecture variable will assign first  
       *    - next to exercise variable 	
       *    - lastly, group exercise variable
       */
      val varWithPriority = slotLecture.values.flatten.map { x => (x,1) } ++  
                      slotExercise.values.flatten.map { x => (x,2) } ++ 
                      subgroupExerciseVariable.values.flatten.map { x => (x,40) }
      def varHeuristic(x:CPIntVar,prior:Int) : Int = {
          if(prior == 40){
              x.size * prior + mapSubGroupPriority(x)
          }else{
              x.size * prior //- x.constraintDegree
          }
      }
      
      val slotScheduleVar = (slotLecture.values.flatten ++ slotExercise.values.flatten).toSet
      
      println(" optimizeObjective " + optimizeObjective + " custom search ")
      search {
            val rand = scala.util.Random
            selectMin(varWithPriority)(!_._1.isBound){case (x,prior) => varHeuristic(x, prior)} match {
             case None => noAlternative
             case Some((x,score)) => {
               val v = x.randomValue(rand)          
               branch(add(x == v))(add(x != v))
               
             } 
          }
            
      }
}

trait DomSearch extends SearchLauncher{
  val allVar = slotLecture.values.flatten ++  slotExercise.values.flatten ++ 
                                                subgroupExerciseVariable.values.flatten
  println("Dom heuristic ")                                           
    search {
        val rand = scala.util.Random
        selectMin(allVar)(!_.isBound){x => x.size} match {
         case None => noAlternative
         case Some(x) => {
            var v = x.randomValue(rand)   
            if(optimizeObjective == 2 && isGreedy_nonPreferredSlot)
              v =  valueHeuristic_ConflictOfProf(x)
             
            branch(add(x == v))(add(x != v))
           } 
        }
     }                                              
}

trait BinaryConflictSearch extends SearchLauncher{
  val allVar = (slotLecture.values.flatten ++  slotExercise.values.flatten ++ 
                                                subgroupExerciseVariable.values.flatten).toArray
//  println("Binary conflict heuristic ")                                           
    search {
        val rand = scala.util.Random
        def valHeuristic(x:CPIntVar) : Int ={
            var v = x.randomValue(rand)  
            if(optimizeObjective == 2 && isGreedy_nonPreferredSlot)
              v =  valueHeuristic_ConflictOfProf(x)
              return v
        }

        def varHeuristic(x:CPIntVar) : Int ={
            x.size/x.constraintDegree
        }
        binaryLastConflict(allVar, i => varHeuristic(allVar(i)), i => valHeuristic(allVar(i)) )
     }                                              
}

trait DomDeg_Heuristic extends SearchLauncher{
    val allVar = slotLecture.values.flatten ++  slotExercise.values.flatten ++ 
                                                subgroupExerciseVariable.values.flatten
//    println("Dom/deg heuristic ")                                           
    search {
        val rand = scala.util.Random
        selectMin(allVar)(!_.isBound){x => x.size/x.constraintDegree.toFloat} match {
         case None => noAlternative
         case Some(x) => {
            var v = x.randomValue(rand)   
            if(optimizeObjective == 2 && isGreedy_nonPreferredSlot)
              v =  valueHeuristic_ConflictOfProf(x)
             
            branch(add(x == v))(add(x != v))
           } 
        }
     }
}
