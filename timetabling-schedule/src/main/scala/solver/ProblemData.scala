package solver

import scala.collection.immutable.ListMap
import scala.collection.mutable.Map
//import scala.collection.mutable.Set

/**
 * @author CongHoa
 * Class contains data problem 
 */
class ProblemData() {
    var semester:String = "Q5"
    var nDay = 5
    var nSlotPerDay = 4
    var nSlots = () => { nDay * nSlotPerDay }
    var maxActivities = 50
    var marginStudentInExercise = 0.8f
    /*
     *  Map representing the program contains list courses :
     */
    val program : Map[String,Set[String]] = Map()
    /*
     *  Map representing the courses in a program:
     */
    val courseInProgam = Map[String,Set[String]]()
    /*
     *  Map representing the number of lecture in each course:
     */
    val nbLecture = Map[String,Int]()
    /*
     *  Map representing the number of exercise in each course,
     *          and the number of mandatory exercise session in each course.
     *  For each course, nbSubcriptionExerOfCourse <= nbExerciseOfCourse
     */
    val nbExerciseOfCourse = Map[String,(Int,Int)]()
    /*
     * Set contains courses in all the programs 
     */
    var courses = Set[String]()
    /*
     * Map representing list of responsible professor the each course
     */
    val courseGivenByProfessor = Map[String,Set[String]]()
    /*
     * Map representing set of courses which a professor is teaching
     */
    val professorTeachingCourse = Map[String,Set[String]]()
    //TODO currently exerciseHandledByProfessor and professorHandleExercise are empty
    /*
     * Map representing list of responsible professor the each course
     */
    val exerciseHandledByProfessor = Map[String,Set[String]]()
    /*
     * Map representing list of responsible professor the each course
     */
    val professorHandleExercise = Map[String,Set[String]]()
    /*
     * Map representing set of unavailable slot of a professor 
     */
    val unavailableTimeOfProf = Map[String,Array[Int]]()
    /*
     * Map representing set of low slot of a professor 
     */
    val lowTimeOfProf = Map[String,Array[Int]]()
    /*
     * Map representing course with fixed lecture sessions  
     */
    val fixedLectureOfCourse = Map[String,Array[Int]]()
    /*
     * Map representing course with fixed exercise sessions  
     */
    val fixedExerciseOfCourse = Map[String,Array[Int]]()
    /*
     * Map represents number of students in each group 
     */
    val nbStudentPerGroup = Map[String,Float]()
    /*
     * Set of continuous courses. 
     * Courses which have many lecture sessions, and to schedule these session sequentially
     */
    var continuousCourses = Set[String]()
    /*
     * Set of course has exercise parts
     */
    var courseHasExercise = Set[String]()
    /*
     * Map represents number exercise of course
     */
    val nbExercise = Map[String,Int]()
    val nbSubcriptionExercise = Map[String,Int]()
    
    def updateCourseInfo(){
        for((course,(nbSubcription,nbSession)) <- nbExerciseOfCourse){
            nbExercise += course -> nbSession
            nbSubcriptionExercise += course -> nbSubcription
        }
        courseHasExercise = nbExercise.filter{case (course,nbSession) => nbSession > 0}.keys.toSet
    }
    
    def courseInProgramStatistic(){
        val fullInfoCourse = courseInProgam.map{case (course, programs) => (course,programs.size)}.toSeq
        val sortedInfo = ListMap(fullInfoCourse.toSeq.sortBy(_._2):_*)
        for((course,nbProgram) <- sortedInfo){
            println(course + " " + nbProgram + "\t " + courseInProgam(course).mkString("\t "))
        }
    }
    
    def checkData(){
        println("********************* 1. Program ****************")
        for((c,k) <- program)
          println(c + " : " + k.toString())
        println("********************* 2. courseInProgam ****************")
        for((c,k) <- courseInProgam)
          println(c + " : " + k.toString())
          
        println("********************* 3. Number lecture Of Course ****************")
        for((c,k) <- nbLecture)
          println(c + " : " + k.toString())
          
        println("********************* 4. Number exercise Of Course ****************")
        for((c,(subcription, nbExercise)) <- nbExerciseOfCourse)
          println(c + " : " + subcription.toString() + " " + nbExercise)
        
          println("********************* 5.Courses ****************")
        for(c <- courses)
          println(c)
          
          println("********************* 6.Course given by prof ****************")
        for((c,k) <- courseGivenByProfessor)
          println(c + " : " + k.toString())
          
           println("********************* 7.Professor teaches the Course  ****************")
        for((c,k) <- professorTeachingCourse)
          println(c + " : " + k.toString())
          
          println("********************* 8. Exercise handled by prof ****************")
        for((c,k) <- exerciseHandledByProfessor)
          println(c + " : " + k.toString())
          
           println("********************* 9.Professor handle the exercise  ****************")
        for((c,k) <- professorHandleExercise)
          println(c + " : " + k.toString())
          
          println("********************* 10. Unavailable Time of Professor  ****************")
        for((c,k) <- unavailableTimeOfProf)
          println(c + " : " +  k.mkString(" "))
          
//          println("********************* 11. Diff of Professor  ****************")
//          val diffProf = professorTeachingCourse.keySet -- unavailableTimeOfProf.keySet
//          diffProf.map { println(_)}
          
//          println("********************* 11. Low Time of Professor  ****************")
//        for((c,k) <- lowTimeOfProf)
//          println(c + " : " +  k.mkString(" "))
//          
          println("********************* 12. Fixed Lecture of Course ****************")
        for((c,k) <- fixedLectureOfCourse)
          println(c + " : " +  k.mkString(" "))
          
          println("********************* 13. Fixed exercise Of Course ****************")
        for((c,k) <- fixedExerciseOfCourse)
          println(c + " : " +  k.mkString(" "))
          
            println("********************* 14. Number of Student Per group ****************")
        for((c,k) <- nbStudentPerGroup)
          println(c + " : " +  k)
    }

    //TODO clear every variable
    def resetInstance(){
        program.clear()
        courseInProgam.clear()
        nbLecture.clear()
    }
    
}