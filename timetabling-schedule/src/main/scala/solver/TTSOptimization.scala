package solver

import oscar.cp._
import oscar.util._

import scala.collection.mutable._

/*
 * Definition of all objectives needed to optimized 
 */

trait OptimizationSubGroupViolation extends PartitionToSubGroup {
    // weighted with number student in each sub group
    val objective1 = sum (for(group <- groupStudent;i <- 0 until nbSubGroups(group))
                                  yield subGroupViolation(group,i) * nbStudentSubgroup(group,i) )
    val totalConflictSchedule = objective1    
    /***********************************************************************/
    /*      Optimize the balance between exercise sessions                 */
    /***********************************************************************/
    val deviationOfExerciseSessions = Map[String,CPIntVar]()
    for((course,loads) <- nbStuInExerciseSessionVar){
        val diffOfSessions = loads.map { x => absolute(x - avgStuPerExerciseSession(course)) }
        deviationOfExerciseSessions += course -> sum(diffOfSessions)
    }
    val totDeviation = sum(deviationOfExerciseSessions.values)
    val nbStuInPartialExercise = partialSubscriptionExercise.toArray
                                              .map {course => totStuLectureSession(course)}.sum
    
    cp.addDecisionVariables(deviationOfExerciseSessions.values)
    
    /* Store the number students in each exercise session in the solution */
    onSolution{
          for((course,loads) <- nbStuInExerciseSessionVar){
              val studInSessions = loads.map{x => x.value}
              nbStuInExerciseSession += course -> studInSessions
          }
      }
}

/*
 * Optimize total conflict in low time of all professors 
 */
trait OptimizationTotalConflictOfProf extends TTSModelObjectives {
    
    /*
     * Sub-Objective 1 : maximum conflict of a professor
     */
    var subObjective1 = CPIntVar(0)
    /*
     * Sub-Objective 2 : total conflict in non preferred slots of all professors 
     */
    var subObjective2 =  CPIntVar(0)
    
    /* Objective 4 : the weighted sum between objective2 and objective3 */
    var objective2 = CPIntVar(0)
    var isMinimizeLowTimeConflict = false
    
    /* If professor has his non-preferred slots then need to optimize conflict in these slots */
    if(nbConflictOfProfessor.values.size > 0 ){ 
        /* Objective 2 : maximum conflict of a professor */
        subObjective1 =  maximum(nbConflictOfProfessor.values)
        
        /* objective3: calculate total conflict in low time of all professors */
        subObjective2 = sum(nbConflictOfProfessor.values) 
        /* total number of sessions of all professors */
        val UBsubObjective2 = conflictLowtimeOfProfessors.map{case (p,vars) => vars.size}.sum
        
        /* Objective 4 : the weighted sum between objective2 and objective3 */
        objective2 = subObjective1 * UBsubObjective2 + subObjective2
        isMinimizeLowTimeConflict = true
    }
}

/* 
 * Optimize maximum conflict low time of a professor 
 * Then minimize number of professors who has the maximum conflict
 */
trait OptimizationMaximumConflictOfProf extends TTSModelObjectives {
    /*
     * Objective 2 : maximum conflict of professors 
     */
    val objective2 = maximum(nbConflictOfProfessor.values)
    val UBObjective2 = (conflictLowtimeOfProfessors.map{case (p,vars) => vars.size}).max
//    val LBObjective2 = objective2.min
//    println("maximum conflict of professors " + objective2 + ", " + UBObjective2)
    /*
     * Objective 3 : number of professors who has the maximum conflict
     */
    /* calculate number of professor who has the maximum conflict */
    val objective3 = CPIntVar(0 until professors.size)
      add(countEq(objective3, nbConflictOfProfessor.values.toArray, objective2))
    
    /*
     * Objective 4 : total conflict in low time of all professors 
     */
    val objective4 = if(nbConflictOfProfessor.values.size > 0 ) 
                          sum(nbConflictOfProfessor.values)  else CPIntVar(0)
}

/*
 * Optimize quality of schedule: maximize the quality of each lecture session and exercise session
 */
trait OptimizationQualitySchedule extends TTSModelObjectives {
    val valueLecture = sum(valueLectureSessions)
    val valueExercise = sum(valueExerciseSessions)
    
    val objective5 = -valueLecture
    val objective6 = -valueExercise
    val objective7 = objective5 * 2 + objective6
    val UBObjective7 = nbStuInAllLectures * 2 + nbStudeInAllExercises
}

/*
 * Optimize quality of teaching unit: minimize number of lectures and exercises occurring in the end day slots
 */
trait OptimizationQualityInEndDaySlots extends TTSModelObjectives {
    val objective8 = nbStuInEndDaySlotsOfLecture
    val objective9 = nbStuInEndDaySlotsOfExercise
    val objective10 = objective8 * 2 + objective9
    val UBObjective10 = nbStuInAllLectures * 2 + nbStudeInAllExercises
}

trait TTSOptimization extends OptimizationSubGroupViolation with OptimizationTotalConflictOfProf
                     with OptimizationQualitySchedule with OptimizationQualityInEndDaySlots{
    val allObjectives = Array[CPIntVar](objective1,subObjective1,subObjective2,objective2,
                              objective5,objective6, objective7,objective8,objective9,objective10, totDeviation)
    val bestObjectives = Array.fill(allObjectives.size)(Int.MaxValue)        
    
    cp.minimize(allObjectives : _*)
    
    for(obj <- allObjectives)
      solver.obj(obj).tightenMode = TightenType.NoTighten
    cp.obj(objective1).tightenMode = TightenType.StrongTighten
    
    cp.addDecisionVariables(allObjectives)
    
    var qualityOfObjective5 = 0f
    var qualityOfObjective7 = 0f
    
    var qualityOfObjective10 = 0f
    
    onSolution{
        for(i <- 0 until allObjectives.size)
            bestObjectives(i) = allObjectives(i).value
        
        qualityOfObjective5 = (objective5.value.toFloat/nbStuInAllLectures / 10).abs
        qualityOfObjective7 = (objective7.value.toFloat/ UBObjective7 / 10).abs
        
        qualityOfObjective10 = objective10.value.toFloat/ UBObjective10 
    }
}

