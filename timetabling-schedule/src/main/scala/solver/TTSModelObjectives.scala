package solver

import oscar.cp._
import oscar.util._

import scala.collection.mutable._

/*
 * Model to calculate objectives:
 *  - conflict in lowtime of professor
 */
trait TTSModelObjectives extends TTSModel {
    /**********************************************************/
    /*      Conflict in lowtime of professor                  */
    /**********************************************************/
    val conflictLowtimeOfProfessors = Map[String,Array[CPIntVar]]()
    /*
     * Teaching unit is defined by course and the index session of course
     * The conflict in low time of each teaching unit causes by the sum conflict of each responsible professor 
     */
    val conflictLowtimeOfLectures = Map[(String,Int), ArrayBuffer[CPIntVar]]()
    
    /* Calculate the conflict in low time of each professor */
    for(p <- professors; if data.lowTimeOfProf.contains(p) && data.lowTimeOfProf(p).size > 0 ){
        val lowTimeOfProf = data.lowTimeOfProf(p)
        val conflictOfProf = ArrayBuffer[CPIntVar]()
        val profHasCourses = data.professorTeachingCourse.getOrElse(p, Set())
        
        /* Calculate the conflict in low time of each teaching unit of professor */
        for(course <- profHasCourses; index <- 0 until data.nbLecture(course)){
            /*
             * Teaching session of each professor can be whether in his non-preferred slots  
             */
            val isInLowTime =   sum(0 until lowTimeOfProf.size)(i => 
                                                   slotLecture(course)(index) === lowTimeOfProf(i))
          
            conflictOfProf.append(isInLowTime)
            
            if(conflictLowtimeOfLectures.contains((course,index)) )
                conflictLowtimeOfLectures((course,index)).append(isInLowTime)
            else
                conflictLowtimeOfLectures += (course,index) -> ArrayBuffer(isInLowTime)
        }
        conflictLowtimeOfProfessors += p -> conflictOfProf
    }
    
    /* Number conflicts in low time of each professor */
    val nbConflictOfProfessor = conflictLowtimeOfProfessors.map{case (p,vars) =>
                                                    if (vars.size > 1) (p,sum(vars)) else (p,vars(0))}
    
    /* Total conflict in low time of each teaching unit */
    val nbConflictLowtimeOfLecture = conflictLowtimeOfLectures.map{case ((c,index),vars) =>
                                    if (vars.size > 1) ((c,index),sum(vars)) else ((c,index),vars(0))}
    
    cp.addDecisionVariables(nbConflictLowtimeOfLecture.values)
    cp.addDecisionVariables(subGroupViolation.values.toArray)
    
    /*********************************************/
    /*      Quality of timetable schedule 			 */
    /*********************************************/
    /* Using an array */
    val qualitySlots = Array(9,10,10,5,      // Monday
                             9,10,10,5,      // Tuesday
                             9,10,10,5,      // Wednesday
                             9,10,10,5,      // Thursday
                             9,10,5,0)      // Friday
    
    /* Calculate quality session of lecture in last slots */
    val qualityLectureVar = slotLecture.map{ case (course,vars) => 
                                          (course,sum(vars.map { session => qualitySlots(session) }) ) }
    
    val qualityExerciseVar = slotExercise.map{ case (course,vars) => 
                                          (course,sum(vars.map { session => qualitySlots(session) })) }
    
    val valueLectureSessions = qualityLectureVar.map{ case (course,qualityLectures) 
                                                            => qualityLectures * totStuLectureSession(course) }
    val valueExerciseSessions = qualityExerciseVar.map{ case (course,qualityExercises) 
                                                            => qualityExercises * avgStuPerExerciseSession(course)}
    
    /* total number of students who attend a lecture   */
    var nbStuInAllLectures = 0
    for( (course,nbSession) <- data.nbLecture){
        nbStuInAllLectures += totStuLectureSession(course) * nbSession
    }
    
    /* total number of students who attend an exercise session   */                                         
    var nbStudeInAllExercises = 0
    for(course <- data.courseHasExercise){
        nbStudeInAllExercises += avgStuPerExerciseSession(course) * data.nbExercise(course)
    }
    /***********************************************************************/
    /*      Quality of timetable schedule  in end day slots                */
    /***********************************************************************/
    /*
     * To simple: reduce number student who has course in the last slots of days
     */
    val endDaySlots = Array( 0,0,0,1,      // Monday
                             0,0,0,1,      // Tuesday
                             0,0,0,1,      // Wednesday
                             0,0,0,1,      // Thursday
                             0,0,0,1)      // Friday                          
     val lectureInEndDaySlots = slotLecture.map{ case (course,vars) => 
                                          (course,sum(vars.map { session => endDaySlots(session) }) ) }
     val exerciseInEndDaySlots = slotExercise.map{ case (course,vars) => 
                                          (course,sum(vars.map { session => endDaySlots(session) }) ) }  
     val nbStuInEndDaySlotsOfExercise = sum(exerciseInEndDaySlots.map{
                                            case (course,nbSessionInEndDay) => nbSessionInEndDay * avgStuPerExerciseSession(course) })
    
     val nbStuInEndDaySlotsOfLecture = sum(lectureInEndDaySlots.map{
                                            case (course,nbSessionInEndDay) => nbSessionInEndDay * totStuLectureSession(course) })
     
    
    /**********************************************/
    /*         Statistic data problem             */
    /**********************************************/      
    def statisticProfUnAvailableTime(){
        for((prof,unAvaiSlots) <- data.unavailableTimeOfProf){
            println(prof + ": " + unAvaiSlots.size + " unavailable, " + professorSchedule(prof).size +  " teaching slots," + (20 - unAvaiSlots.size)) 
            println("\t" + unAvaiSlots.mkString(",") + " - " + data.lowTimeOfProf.getOrElse(prof,Array()).mkString(",") )
        }
    }
     
    def checkingNonPreferedSlot(){
        for(prof <- professors){
            val unAvaiSlots = data.unavailableTimeOfProf.getOrElse(prof, Array())
            val lowTimeSlots = data.lowTimeOfProf.getOrElse(prof, Array())
            var nbDup = 0 
            lowTimeSlots.foreach( s => if (unAvaiSlots.contains(s) ) nbDup +=  1  )
            if (nbDup > 0)
              println(prof + "\t" + nbDup + "number duplicate non-preferred slots ")
        }
    }
     
}