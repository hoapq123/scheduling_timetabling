package solver

import oscar.cp.core.variables.CPIntVar
import oscar.cp.core.Constraint
import oscar.cp.core.CPOutcome
import oscar.cp.core.CPOutcome._
import oscar.cp.core.CPPropagStrength
import oscar.algo.reversible.ReversibleInt
import scala.collection.mutable.Map
//import scala.collection.mutable.Set

/* Constraint doing:
 *  - Check the possibility of the balancing between exercise sessions
 *  - Removing the slots in which number of available students is smaller than the expected minimum
 *  	Unavailable students are who have the lectures or full subscription exercise at this slot
 */

class TTSConstraint(slotLecture:Map[String,Array[CPIntVar]]
      , slotMultipleExercise:Map[String,Array[CPIntVar]], slotUniExercise:Map[String,Array[CPIntVar]]
      , nbStudent: Map[String,Int], courseInProgram: Map[String,Set[String]] 
      , avgStuPerExerciseSession : Map[String,Int]
      , marginStudentInExercise: Float) 
          extends Constraint(slotLecture.head._2(0).store, "TimeTableConstraint") {
  
      val isPrunning = new ReversibleInt(s, 0)
      val varLecture = slotLecture.values.flatten
      val varMultipleExercise = slotMultipleExercise.values.flatten
      
      // data of problem
      val courses   = courseInProgram.keys
      val groupIds  = Map[String,Int]()    
      val nSlots    = 20
      
      val nbGroup = nbStudent.size
      val nbCourse = courses.size
      val groupAvailable = Array.fill(nbGroup,nSlots)(1)
      
      override def setup(l: CPPropagStrength): CPOutcome = {
          /*
           * Map group with index 
           */
          val groupStudent = nbStudent.keys
          for((group,index) <- groupStudent.zipWithIndex){
              groupIds += group -> index
          }
          
          varLecture.map(x => x.callPropagateWhenBind(this))
          varMultipleExercise.map(x => x.callPropagateWhenBind(this))
          Suspend
      }
      
      override def propagate(): CPOutcome = {
          var fullSubcriptionNotBound = varLecture.filter { x => !x.isBound } ++ varMultipleExercise.filter { x => !x.isBound }
          /*
           * if all lectures and multiple exercise are bounded
           * then do the pruning only once time
           */
          if(isPrunning.getValue() == 0 && fullSubcriptionNotBound.isEmpty){  
              isPrunning.setValue(1)
              
              // reset group schedule
              for(g <- 0 until nbGroup; s <- 0 until  nSlots) groupAvailable(g)(s) = 1
              assignGroupSchedule()
              if (pruneUniExerciseVariable == Failure ) return Failure
          }
          Suspend
      }
      
      /*
       * Initialize group schedule and assign slot of course and full subscription exercise
       */
      private def assignGroupSchedule(){
          // 1. assign slot of course 
          for((course,vars ) <- slotLecture){
              val groupIdOfCourse = courseInProgram(course).map( g => groupIds(g))
              for(gId <- groupIdOfCourse; x <- vars){
                 groupAvailable(gId)(x.value) = 0
             }
          }
          
          // 2. assign full subscription exercise
          for((course,vars ) <- slotMultipleExercise){
              val groupIdOfCourse = courseInProgram(course).map( g => groupIds(g))
              for(gId <- groupIdOfCourse; x <- vars){
                 groupAvailable(gId)(x.value) = 0
             }
          }
      }
          
      /*
       * 2. Partial Exercise: If it variable is unbound then remove invalid value
       */
      private def pruneUniExerciseVariable(): CPOutcome = {
          for((course,vars) <- slotUniExercise){
              val minStudentPerSlot = (avgStuPerExerciseSession(course) * marginStudentInExercise ).toInt
              // get value in domain of slot exercise
              val allValue = scala.collection.mutable.Set[Int]()
              for(x <- vars; if !x.isBound){ x.map { v => allValue.add(v) } }
              
              // for each slot in domain, if the total student available is smaller than minimum required 
              // then remove this value in the domain
              for(v <- allValue){  
                  var totalStudentAvailable = 0
                  for(g <- courseInProgram(course)){
                     totalStudentAvailable += groupAvailable(groupIds(g))(v) * nbStudent(g)
                  }
                  if(totalStudentAvailable < minStudentPerSlot){
                      // remove value in the domain
                      for(x <- vars; if x.hasValue(v) ){
//                          println("TTS " + course + "\t" + x)
//                            println("\t slot: " + v + " - minimum: " + minStudentPerSlot + " - total available " + totalStudentAvailable)
                          if (x.removeValue(v) == Failure) return Failure
                      }
                  }
              }
          }
          Suspend
      }
      
      /*
       * 3. Check whether the bounded partial exercise variable can have enough number of students to attend
       */
      private def checkUniExerciseVariable(): CPOutcome = {
        
          for((course,vars) <- slotUniExercise;x <- vars; if x.isBound){
              val minStudentPerSlot = (avgStuPerExerciseSession(course) * marginStudentInExercise ).toInt
              var totalStudentAvailable = 0
              val v = x.min
              /* calculate all students who are available in assigned slot */
              for(g <- courseInProgram(course)){
                 totalStudentAvailable += groupAvailable(groupIds(g))(v) * nbStudent(g)
              }
              /* if number avaiable students are small than minimum require then backtrack */
              if(totalStudentAvailable < minStudentPerSlot){
                  println("remove exercise " + course + " " + v) 
                  return Failure
              }
          }
          Suspend
      }
    
  
}

object TTSConstraint{
     def apply(slotLecture:Map[String,Array[CPIntVar]]
      , slotMultipleExercise:Map[String,Array[CPIntVar]], slotUniExercise:Map[String,Array[CPIntVar]]
      , nbStudent: Map[String,Int], courseInProgram: Map[String,Set[String]] 
      , avgStuPerExerciseSession : Map[String,Int], marginStudentInExercise: Float) 
     
            = {new TTSConstraint(slotLecture,slotMultipleExercise,slotUniExercise,
                        nbStudent,courseInProgram, avgStuPerExerciseSession, marginStudentInExercise)}
}