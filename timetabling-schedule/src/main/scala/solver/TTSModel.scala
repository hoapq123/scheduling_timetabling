
package solver


import oscar.cp._
import oscar.util._

import oscar.algo.search.SearchStatistics

import scala.util.Sorting
import scala.math.Ordered
import scala.Array.canBuildFrom
import scala.collection.mutable.Map
import scala.collection.mutable.ArrayBuffer

/**
 * @author CongHoa
 * This is constraint programming model of time table problem
 */
class TTSModel(val data:ProblemData,val rateRoundStudent: Int = 10,val pcNonPreferredSlot : Float = 0.2f) extends CPModel{
      /**********************************************/
      /*       Solver configuration                 */
      /**********************************************/
      val cp = solver
      cp.silent = true
      var lastSolutionStat: SearchStatistics = null
      /**********************************************/
      /*              Parameter                     */
      /**********************************************/
      val nSlots = data.nSlots()
      val rSlots = 0 until nSlots 
      /*
       * This is a parameter from the user input
       *   If margin =  1: Number of students in each exercise sessions of same course 
       *   								 need be strictly balanced
       *   If margin =  0: Number of students in each exercise sessions of same course are free
       *   e.i : Course with 2 session of exercise and only 1 is mandatory
       *   		If marginStudentInExercise = 1 then, 50% of student need for slot 1 and 50% of student need for slot 2
       *   		If marginStudentInExercise = 0 then, it is free, may be 100% of student need for slot 1 and 0% of student need for slot 2
       */
      var margin = 0.8f
      
      /* Contain the index of current optimizing objective */
      var optimizeObjective = 1  
      
      /* Set of pairs continuous slots */
      val continousSlots = rSlots.filter { i => i%2 == 0 }. map{i => (i, i + 1)}
      
      // Data of problem
      val groupStudent = data.program.keySet
      val courses = data.courseInProgam.keySet
      val professors  =  data.professorTeachingCourse.keySet ++ data.professorHandleExercise.keySet
      val nbStudent = data.nbStudentPerGroup.map{case (g,exactNb) => (g, (exactNb * rateRoundStudent).round ) }
      
//      val nbStudent = data.nbStudentPerGroup.map{case (g,exactNb) => (g, exactNb.round ) }
      
      val partialSubscriptionExercise = data.courseHasExercise.filter{ course =>
                                                    data.nbSubcriptionExercise(course) < data.nbExercise(course)}
      val fullSubscriptionExercise  = data.courseHasExercise.filter{ course =>
                                                    data.nbSubcriptionExercise(course) == data.nbExercise(course)}
      /*****************************************************/
      /*         Data for Partition Groups Into Smaller    */
      /*****************************************************/
      val nbStuInExerciseSession = Map[String,Array[Int]]()
      
      // number subgroup for each group
      val nbSubGroups = Map[String,Int]()
      
      /* each subgroup identified by its parent group name and its index in group */
      val subGroupViolation = Map[(String,Int),CPIntVar]()
      val nbStudentSubgroup = Map[(String,Int),Int]()
      val subGroupSchedule = Map[(String,Int), Array[CPIntVar]]()
      
      /* subgroup exercise variables are grouped into its corresponding course */
      val subgroupExerciseVariable = Map[String, Array[CPIntVar]]() 
      /**********************************************/
      /*         Decision variable                  */
      /**********************************************/
      val decisionVariables = ArrayBuffer[CPIntVar]() 
      
      /* Course sessions */
      val slotLecture = Map[String, Array[CPIntVar]]()
      val slotExercise = Map[String, Array[CPIntVar]]()
      
      // Initialize variable of lecture sessions
      for((course,nbSession) <- data.nbLecture){
          val sessionVar = Array.tabulate(nbSession )(i => CPIntVar(rSlots, name = course + i))
          slotLecture += course -> sessionVar
      }
      
      // Initialize variable of exercise sessions
      for(course <- data.courseHasExercise){
          val nbSession = data.nbExercise(course)
          val sessionVar = Array.tabulate(nbSession )(i => CPIntVar(rSlots, name = course + ":ex:" + i))
          slotExercise += course -> sessionVar
      }
      
      decisionVariables ++= slotLecture.values.flatten ++ slotExercise.values.flatten
           
      /*
       * Group schedule includes : lecture and full subscription exercise sessions
       */
      val groupSchedule = Map[String, Array[CPIntVar]]()
      /*
       *  Mapping variables course sessions to group schedule
       */
      for((group,courseOfGroup) <- data.program){
          val sessionOfGroup = courseOfGroup.flatMap(c => slotLecture(c)).toArray
          // get slot exercises in which each group have to attend all full session
          val mutipleExerciseOfGroup =  courseOfGroup.filter { c => fullSubscriptionExercise.contains(c) }
                                                     .flatMap { c => slotExercise(c) }.toArray 
          groupSchedule += group -> (sessionOfGroup ++ mutipleExerciseOfGroup)                                             
      }
      
    
      /**********************************************/
      /*         Model Variable                     */
      /**********************************************/
      /*
       * Calculate the conflict schedule of each group
       */
      val nbSessionOfGroup = Map[String,Int]()
      for((group,course) <- data.program){
          val nbSession = course.toArray.map(c => data.nbLecture(c) + data.nbExerciseOfCourse.getOrElse(c, (0,0))._1).sum
          nbSessionOfGroup += group -> nbSession
      }
      val maxNbSessionOfGroup = nbSessionOfGroup.values.max
      val groupViolation = Map[String,CPIntVar]()
      
      for((group,nbSession) <- nbSessionOfGroup){
          groupViolation += group -> CPIntVar(0 to nbSession, "violation of " + group)
      }
      
      /*
       * Professor schedule
       */
      val professorSchedule = Map[String,Array[CPIntVar]]()
      for(p <- professors){
          val sessionLecture = data.professorTeachingCourse.getOrElse(p, Set()).flatMap { slotLecture(_)}
          val sessionExercise = data.professorHandleExercise.getOrElse(p, Set()).flatMap( slotExercise(_) ) 
          val sessionOfProfessor = sessionLecture ++ sessionExercise
          professorSchedule += p -> sessionOfProfessor.toArray
      }
      
      /**********************************************/
      /*         Constraint                         */
      /**********************************************/
      /*
       * C1: Lecture and exercise session of the same course are on different slots
       */
      for(c <- courses){
          val allSessionOfCourse = slotLecture(c) ++ slotExercise.getOrElse(c, Array())
          if(allSessionOfCourse.size > 1)
              add(allDifferent(allSessionOfCourse),Strong)
      }
      
      /*
       * C2: A professor can not teach 2 different sessions at the same time
       */
      for(p <- professors; if p.trim() != ""){
          val sessionOfProfessor = professorSchedule.getOrElse(p, Array())
          if(sessionOfProfessor.size > 1){
               add(allDifferent(sessionOfProfessor),Strong)
          }
      }
      
      /*
       * C3: No lecture or exercise occurs in the unavailable slots of the responsible professors
       */
      for((p,unvailableSlot) <- data.unavailableTimeOfProf; if professors.contains(p)){
          val sessionOfProfessor = professorSchedule.getOrElse(p, Array()) 
              
          for(sessionVar <- sessionOfProfessor)
              unvailableSlot.map(un => add(sessionVar != un))
      }

      /*
       * C4: Limitation of number courses occurring at the same time
       */
      val allSessions =  ( slotLecture.values ++ slotExercise.values).toArray 
      add(gcc(allSessions.flatten, rSlots,0,data.maxActivities),Weak)
      
      /*
       * C5: fixed lecture and exercise for courses 
       */
      for((course,fixedSlots) <- data.fixedLectureOfCourse){
          Sorting.quickSort(fixedSlots)
          for(i <- 0 until math.min(fixedSlots.size,data.nbLecture(course)) ){
              add(slotLecture(course)(i) ==  fixedSlots(i))
          }
      }
      for((course,fixedSlots) <- data.fixedExerciseOfCourse){
          Sorting.quickSort(fixedSlots)
          for(i <- 0 until fixedSlots.size){
              add(slotExercise(course)(i) ==  fixedSlots(i))
          }
      }
      
      /*
       * C6: Continuous sessions
       */
      for(course <- data.continuousCourses){
//          println("continuousCourses " + course) && data.nbExercise(course) == 1
          if(data.nbLecture(course) == 1){
              add(table(slotLecture(course)(0), slotExercise(course)(0), continousSlots))
          }else if(data.nbLecture(course) == 2){
              add(table(slotLecture(course)(0), slotLecture(course)(1), continousSlots))
          }else{
              println(" data continuous course not correct: " + course 
                        + ": lecture " + data.nbLecture(course) + ",exercise " + data.nbExercise(course) )
          }
      }
      /*
       * C8: Break symmetry between lecture sessions or exercise sessions of the same course
       */
      for((course,lectureSessions) <- slotLecture; if !data.continuousCourses.contains(course)){
          val from = data.fixedLectureOfCourse.getOrElse(course, Array()).size
          // all the non-fixed lecture sessions of course
          if( lectureSessions.size - from > 1)
              for(i <- from until lectureSessions.size -1){
                  add(lectureSessions(i) < lectureSessions(i+1))
              }
      }
      
      for((course,exerciseSessions) <- slotExercise){
          val from = data.fixedExerciseOfCourse.getOrElse(course, Array()).size
          // all the non-fixed exercise sessions of course
          if( exerciseSessions.size - from > 1)
              for(i <- from until exerciseSessions.size -1){
                  add(exerciseSessions(i) < exerciseSessions(i+1))
              }
      }
      
      /*
       * Calculate  number of students on average attending each course
       */
      val totStuLectureSession = data.courseInProgam.map{ case(course,groups) 
                                                    => (course,groups.toArray.map(g => nbStudent(g)).sum)}
      val totStuExerciseSession = totStuLectureSession.filter{case(course,groups) 
                                                    => data.courseHasExercise.contains(course) }
        
      val avgStuPerExerciseSession = Map[String,Int]()    // data format: course, nbStudent
      for(course <- data.courseHasExercise){
          /*
           * Course which has uni exercise then nbSubcription = 1, avg = totalStudent/nbExercise
           *  - has multiple exercises then nbSubcription = nbExercise, avg = totalStudent
           */
          val avg = totStuLectureSession(course)/data.nbExercise(course) * data.nbSubcriptionExercise(course)
          avgStuPerExerciseSession += course -> avg
      }
      
      val slotFullSubcriptionExercise = Map[String, Array[CPIntVar]]()
      val slotPartialSubcriptionExercise = Map[String, Array[CPIntVar]]()
      
      fullSubscriptionExercise.map{course => slotFullSubcriptionExercise += course -> slotExercise(course)}
      partialSubscriptionExercise.map{course => slotPartialSubcriptionExercise += course -> slotExercise(course)}
      
      /*
       * Additional constrain: Removing the slots in which number of available students 
       * 										is smaller than the expected minimum 
       */
      add(TTSConstraint(slotLecture,slotFullSubcriptionExercise,slotPartialSubcriptionExercise,
                        nbStudent,data.courseInProgam, avgStuPerExerciseSession, margin))
       
      /**********************************************/
      /*         Statistic data problem             */
      /**********************************************/                 
      def statisticData(){
        
        println("[STATS]******************* TimeTabling ****************")
        println("****** Number groups \t\t\t\t: " + data.program.size )
        println("****** Number students \t\t\t\t: " + nbStudent.values.sum )
        println("****** Number courses \t\t\t\t: " + courses.size 
                              + " ; courses having exercise \t" + data.courseHasExercise .size  )
        println("****** Number lecture sessions: \t" + data.nbLecture.values.sum
                                  + "\t\t exercise " + data.nbExercise.values.sum )
        val manySessions = for( (course,(nbSubcription,nbSession)) <- data.nbExerciseOfCourse; if nbSession > 2) yield ( course + ": " + nbSession)
        println("****** Course with more than 2 exercise sessions : " + manySessions.mkString(","))                              
        println("****** Number professors \t\t\t: " + professors.size)
        val sizeProgram = data.program.values.toArray.map{s => s.size}
        val avgCourse =  BigDecimal(sizeProgram.sum.toFloat / data.program.size ).setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble
        println("****** On average, number courses of group per week : " + avgCourse + " per week ")
        val avgSession =  BigDecimal(nbSessionOfGroup.values.sum.toFloat / data.program.size ).setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble
        println("****** On average, number slots of group per week : " + avgSession + " out of 20 ")
        println("********************************************************")
    }                      
}