package solver

import oscar.cp._
import oscar.util._
import java.io._
import scala.collection.mutable.Map
import scala.util.Random
import scala.collection.immutable.ListMap
/*
 * Display the result and write schedule to file
 */
trait PrintSolution extends TTSRelaxation{
    
    def printSolution{
        printObjectives
        writeOutputSchedule
    }
    
    def printObjectives(){
        println("**************** Result ************")
        println("Total Conflict schedule \t\t\t" + bestObjectives(0).toFloat/rateRoundStudent)
        println("Total Conflict non-preferred slots \t" + bestObjectives(2))
        println("Quality schedule \t\t\t" + (qualityOfObjective7 * 100) )
        println("quality in the end slots \t" + (qualityOfObjective10 * 100) )
        println("Total Deviation\t" + bestObjectives(10).toFloat/rateRoundStudent + "\t, percentage of deviation \t" 
                                           + bestObjectives(10) * 100 /nbStuInPartialExercise  )
    }
    
    def writeOutputSchedule(){
        val dirPath = "output/" + data.semester 
        //1. creating director 
        val theDir = new File(dirPath);
        if (!theDir.exists()) {
            theDir.mkdirs()
            println("create directory")
        }
        import java.text.DecimalFormat 
        val formatter = new DecimalFormat("#.###")
        val qualitySchedule = formatter.format(qualityOfObjective7)
        
        var outFile = dirPath + "/schedule.txt"
        val file = new File(outFile )
        val bw = new BufferedWriter(new FileWriter(file))
                
        var str = "************ Lecture ************"
        bw.write(str)
        bw.append(System.lineSeparator)
        for((course,slots) <- bestLectureSolution){
            str = course + ":\t" + slots.mkString(",")
            bw.write(str)
            bw.append(System.lineSeparator)
        }
        str = "************ Exercise ************"
        bw.write(str)
        bw.append(System.lineSeparator)
        for((course,slots) <- bestExerciseSolution){
            str =  course + ":\t" + slots.mkString(",")
            bw.write(str)
            bw.append(System.lineSeparator)
        }
        bw.close()
    }
    
    /*
     * Real number student in each lecture and exercise: use float number
     */
    def compareResultWithMip(){
        println("**************************************************")
        println("*** compareResultWithMip 	*********")
        val realTotStudentInLecture = Map[String,Float]()
        val realAvgStudentInExercise = Map[String,Float]()
        for((course,groups)  <- data.courseInProgam){
            val realStudentInCourse = groups.toArray.map(g => data.nbStudentPerGroup(g)).sum
            realTotStudentInLecture += course -> realStudentInCourse
            
            if(data.courseHasExercise.contains(course)){
                val (nbSubcription,nbExercise) = data.nbExerciseOfCourse(course)
                realAvgStudentInExercise += course -> (realStudentInCourse * nbSubcription /nbExercise) 
            }
        }
        val realTotStudentInLectureSession = realTotStudentInLecture.map{case (course,nb) 
                                          => nb * data.nbLecture(course)}.toArray.sum
        var realTotStudentInExerciseSession = 0f
        for(course <- data.courseHasExercise){
            realTotStudentInExerciseSession += realAvgStudentInExercise(course) * data.nbExercise(course)
        }
                                          
        /* 
         * Quality of schedule: objective 7
         */ 
        var realQualityLecture = 0.0f                  
        for((course,sessions) <- bestLectureSolution){
            realQualityLecture += sessions.map(s => qualitySlots(s)).toArray.sum * realTotStudentInLecture(course)
        }
        var realQualityExercise = 0.0f  
        for((course,sessions) <- bestExerciseSolution){
            realQualityExercise += sessions.map(s => qualitySlots(s)).toArray.sum * realAvgStudentInExercise(course)
        }
        val qualitySchedule = (realQualityLecture * 2 + realQualityExercise) / 10/
                      ( realTotStudentInLectureSession * 2 +  realTotStudentInExerciseSession)
        println("real qualitySchedule - objective 7 " + qualitySchedule.toString())
        println("realQualityLecture " + realQualityLecture + " , realQualityExercise " +  realQualityExercise)
        
        println("realTotStudentInLectureSession\t " + realTotStudentInLectureSession + "\t" + realTotStudentInExerciseSession)                                  
    }
    
    /*
     * Display the value and run time each time a solution is found
     */
    def objectiveProgress(startTime:Long){
      onSolution{
         val executionTime = java.lang.System.currentTimeMillis() - startTime
         if(optimizeObjective < 2){
             println(executionTime.toFloat/1000 + " s\t" + objective1.value.toFloat/rateRoundStudent )
         }else if(optimizeObjective == 2){
             println(executionTime.toFloat/1000 + " s, \t" + subObjective1.value 
                                 + "\t" + subObjective2.value  +"\t " + objective2.value  )
         }else if(optimizeObjective > 2 && optimizeObjective  < 5){
             println(executionTime/1000 + " s\t" + (qualityOfObjective7 * 100) 
                         + "\t" + (qualityOfObjective10 * 100) )
         }else if(optimizeObjective >= 5){
             println(executionTime/1000 + " s \t" + totDeviation.value.toFloat/rateRoundStudent 
                             + "\t "  + totDeviation.value * 100 /nbStuInPartialExercise  )
         }
      }
    }
    
    /* For search by subgroup */
    def printNbStudentInExerciseOfSubgroup(){
        println ("******** Distribution Students in Exercise Sessions *********")
        for((course,sessions) <- nbStuInExerciseSession){
            println("/****************************** Course " + course + " , avg " + avgStuPerExerciseSession(course))
            sessions.zipWithIndex.map{case (nb,index) => println(index + ": " + nb.toFloat/rateRoundStudent + " stu")}
        }
    }
    
    def printSlotCourses{
          println("/****************************** Solution ****************************/")
          println("/****************************** Slot lecture ****************************/")
          var i = 0
          var sortedSolution = ListMap(bestLectureSolution .toSeq.sortBy(_._1):_*)
          for((course,slot) <- sortedSolution){
              print (course + ": " + slot.mkString(" ") + "\t\t\t")
              i = i + 1
              if(i%2 ==0) {
                  println
                  println()}
              
          }
          println
          println("/****************************** Slot exercise ****************************/")
          i = 0
          sortedSolution = ListMap( bestExerciseSolution.toSeq.sortBy(_._1):_*)
          for((course,slot) <- sortedSolution){
              print (course + ": " + slot.mkString(" ") + "\t\t\t")
              i = i + 1
              if(i%2 ==0) {
                  println
                  println()}
              
          }
          
          //TODO : print schedule of each group
      }
    
    /*
     * SlotLecture : all registered group have to attend => busy
     * SlotExercise: checking  whether the percentage of students who can attend this exercise
     * 							 is greater than the number required to be balance between sessions
     */
    def isPossibleExerciseBalance(){
        println("/****************************** isPossibleExerciseBalance ****************************/")
            /*
             * Map group with index 
             */
            val groupIds = Map[String,Int]()    
            for((group,index) <- groupStudent.zipWithIndex){
                groupIds += group -> index
            }
            /*
             * Put slot schedule to the slot of attending groups
             */
            val nbGroup = groupStudent.size
            val nbCourse = courses.size
            val groupAvailable = Array.fill(nbGroup,nSlots)(1)
            for((course,slots) <- bestLectureSolution){
                 val groupIdOfCourse = data.courseInProgam(course).map( g => groupIds(g))
                 for(gId <- groupIdOfCourse; s <- slots){
                     groupAvailable(gId)(s) = 0
                 }
            }
            for(course <- fullSubscriptionExercise){
                val slots = bestExerciseSolution(course)
                val groupIdOfCourse = data.courseInProgam(course).map( g => groupIds(g))
                 for(gId <- groupIdOfCourse; s <- slots){
                     groupAvailable(gId)(s) = 0
                 }
            }
            /*
             * Check each slot of exercise can be possible balanced
             */
            for((course,slots) <- bestExerciseSolution; if(partialSubscriptionExercise.contains(course))){
                val min = (avgStuPerExerciseSession(course) * margin).toInt
                for(s <- slots){
                    var totalAvailable = 0
                    for(g <- data.courseInProgam(course)){
                       totalAvailable += groupAvailable(groupIds(g))(s) * nbStudent(g)
                    }
                    if(totalAvailable < min){
                      println(course)
                        println("\t slot: " + s + " - minimum: " + min + " - total available " + totalAvailable)
                    }
                }
            }
        }
    
    /*
     * Fix slot lecture of current solution by using constraint
     */
    def fixSolutionWithConstraint(){
          println("/****************************** Fix Slot lecture ****************************/")
          var sortedSolution = ListMap(bestLectureSolution .toSeq.sortBy(_._1):_*)
            for((course,slots) <- sortedSolution){
                for(i <- 0 until slots.size){
                    println("add(slotLecture(\"" + course + "\")(" + i + ") == " + slots(i) + ")")
                }
            }
          println("/****************************** Fix Full Exercises ****************************/")
          for(course <- fullSubscriptionExercise){
              val slots = bestExerciseSolution(course)
              for(i <- 0 until slots.size){
                  println("add(slotExercise(\"" + course + "\")(" + i + ") == " + slots(i) + ")")
              }
              
          }
      }
}