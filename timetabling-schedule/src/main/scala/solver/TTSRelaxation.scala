package solver

import oscar.cp._
import oscar.util._
import scala.util.Random
import scala.collection.mutable.Map
import scala.math.max

trait TTSRelaxation extends SearchLauncher with TTSOptimization {
      
      /*
       * Minimize violation of group schedule. 
       * Not include the balancing between exercise session
       */
      def searchBySubGroup(startTime : Long, maxTime:Int){
          var isFound = false
          /* 
           * Search Parameter 
           */
          var fixPercent = 60
          var limitedBacktrack = 200                           // limited search for semester Q5
          if(data.semester == "Q4Q6")  {
            limitedBacktrack = 10000 
          }
          var timeLastSolution = startTime
          
          // update the time of last solution
          onSolution {
            timeLastSolution = java.lang.System.currentTimeMillis()
          }
          var lowerBound= objective1.min
                        
          /******************************************/
          /* Find the first solution 								*/
          /******************************************/
          while(!isFound && java.lang.System.currentTimeMillis() - startTime < maxTime){
              lastSolutionStat = startSubjectTo(nSols = 1, failureLimit = limitedBacktrack){
        		     add(objective1 <= lowerBound)
          		}
          		isFound = lastSolutionStat.nSols == 1
          		// increase the lower bound when not found the solution
          		if(!isFound){                  
          			nbRestart += 1 * rateRoundStudent /2
          			lowerBound += 1
          		}
          }
          
          
          var runtime = (java.lang.System.currentTimeMillis() - startTime) 
          /**********************************************************/
          /*      Optimize other objectives 			                  */
          /**********************************************************/
          /* 
           * If achieve the best total conflict schedule, move to next objective
           * Change tighten mode for minimizing conflictlowTime
           */
          if(cp.lastSol(objective1) == objective1.min){
              solver.obj(objective1).tightenMode = TightenType.WeakTighten
              
              // if professors have non-preferred slots
              if(isMinimizeLowTimeConflict){
                  optimizeObjective = 2
              }
              // move optimize the quality of schedule
              else{  
                   optimizeObjective = 3
              }
              println("**** change to objective " + optimizeObjective)
          }
          
          while (java.lang.System.currentTimeMillis() - startTime < maxTime) {
              
              lastSolutionStat = startSubjectTo(failureLimit = limitedBacktrack){   
                  /**********************************************************************/ 
                  /* 		1. Set objective priority and change to the next objective			*/
                  /**********************************************************************/ 
                  setObjectivePriority()
                  
                  /**********************************************************************/ 
                  /* 2. Relaxation: Fixing some variables, define the neighborhood			*/
                  /**********************************************************************/ 
                  /*	Optimizing total conflict in student schedule */
                  if(optimizeObjective == 1){
                      fixVar_TotalConflictScheduleOptimization(fixPercent)
                  }
                  /*	Optimizing conflict in low time of professor */
                  if(optimizeObjective == 2){
                      fixVar_LowTimeOptimization(fixPercent)
                  }
                  /*
                   * Fixing variable for optimizing quality of lectures and exercises 
                   * optimizeObjective == 3 : quality of schedule
                   * optimizeObjective == 4 : in end week slots
                   */
                  if(optimizeObjective == 3){
                      fixVar_QualityScheduleOptimization(fixPercent,isInLowQualitySlots(_))
                  }
                  
                  if(optimizeObjective == 4){
                      fixVar_QualityScheduleOptimization(fixPercent,isInEndDaySlots(_))
                  }
                  
                  /* Optimizing the balance between exercise sessions */
                  if(optimizeObjective == 5 ){
                      fixVar_BalancingExerciseSessions(fixPercent)
                  }
              } // end startSubjectTo
              
              
              /* 
               * 3. If search stopped because all subtree has been visited,
               * we enlarge the neighborhood otherwise, we reduce it
               */
              if (lastSolutionStat.completed)
                fixPercent = max(fixPercent - 10, 0)
              else
                fixPercent = max(fixPercent - 3, 0)
              /* If found a solution, we set back the environment to 60 */
              if (lastSolutionStat.nSols >= 1){
                  fixPercent = 60
              }
          }
          
          /**********************************************************************/ 
          /* 		Set objective priority and change to the next objective			*/
          /**********************************************************************/ 
          def setObjectivePriority(){
            /* Optimizing total conflict in student schedule  */
            if(optimizeObjective == 1){
               isChangeToNextObjective(cp.lastSol(objective1) == objective1.min,Array(objective1) ) 
            }
            
            /* Optimizing conflict in non-preferred slots of professor  */
            if(optimizeObjective == 2){
                solver.obj(objective2).tightenMode = TightenType.StrongTighten
                isChangeToNextObjective(cp.lastSol(objective2) == objective2.min,Array(objective2))
            }
            
            /* Optimizing quality of lecture and exercise */
            if(optimizeObjective == 3){
                solver.obj(objective7).tightenMode = TightenType.StrongTighten
                isChangeToNextObjective(cp.lastSol(objective7) == objective7.min, Array(objective7))
            }
            /*
             * Optimizing quality of lecture and exercise in the last slot of day
             */
            if(optimizeObjective == 4){
              if( !isChangeToNextObjective(cp.lastSol(objective10) == objective10.min, Array(objective8,objective10)) ){
                  solver.obj(objective10).tightenMode = TightenType.StrongTighten
              }
            }
            /*
             * Minimizing the total deviation of number students 
             * in partial subscription exercise sessions
             */
            if(optimizeObjective == 5){
                solver.obj(totDeviation).tightenMode = TightenType.StrongTighten
            }
          }
          
          /* 
           * Change the configuration of VO-LNS when moving next objective 
           */
          def isChangeToNextObjective(condition: Boolean,currObjectives: Array[CPIntVar]) : Boolean = {
              // if time is out or condition is met then change to next objective
              if(condition || java.lang.System.currentTimeMillis() - timeLastSolution > maxTime/15){
                  optimizeObjective += 1
                  currObjectives.map(o => solver.obj(o).tightenMode = TightenType.WeakTighten)
                  timeLastSolution = java.lang.System.currentTimeMillis()
                  fixPercent = 60
                  
                  println("**********change objective to " + optimizeObjective)
                  return true
              }
                  
              return false
          }
          
      }
      
      /* 
       * Fixing variable for optimizing conflict in low time of professor
       */
      def fixVar_LowTimeOptimization(fixPercent:Int){
        // fix slot exercise because it has no low time conflict
         for((course,vars) <- slotExercise; if rand.nextInt(100) < fixPercent){
              vars.map{x => add(x == cp.lastSol(x)) }
              if(slotPartialSubcriptionExercise.contains(course))
                subgroupExerciseVariable(course).map{x => add(x == cp.lastSol(x)) }
         }
         // fix slot lecture if it has no low time conflict
         for((course,vars) <- slotLecture; index <- 0 until vars.size){
              val isConflictLowTime = if (nbConflictLowtimeOfLecture.contains(course,index))
                                        cp.lastSol( nbConflictLowtimeOfLecture(course,index) ) else 0
              if(isConflictLowTime == 0 && rand.nextInt(100) < fixPercent){
                 add(vars(index) == cp.lastSol (vars(index))) 
              }
         }
      }
      
      /*
       * Fixing variable for optimizing quality of lectures and exercises 
       * Parameter:
       * 		-	checkViolationFunc: function to check whether sessions is low quality slots
       */
      def fixVar_QualityScheduleOptimization(fixPercent:Int,checkViolationFunc:Int => Boolean){
         /* Fix slot uni session exercise and variable of subgroup attending this course */ 
          for((course,vars) <- slotPartialSubcriptionExercise ){
                val sessionConflict = vars.filter { x => !checkViolationFunc( cp.lastSol(x) ) }
                if(sessionConflict.size == 0 && rand.nextInt(100) < fixPercent){
                    vars.map{x => add(x == cp.lastSol(x)) }
                    subgroupExerciseVariable(course).map{x => add(x == cp.lastSol(x)) }
                }
          }
          /* multiple session exercise */
          val fullSubcription = (slotFullSubcriptionExercise.values.flatten ++ slotLecture.values.flatten)
          for (x <- fullSubcription; if !checkViolationFunc( cp.lastSol(x)) ){
              if(rand.nextInt(100) < fixPercent)
                add(x == cp.lastSol(x))
          }
      }
      
      /*
       * Fixed variables optimizing the balance between exercise sessions
       */
      def fixVar_BalancingExerciseSessions(fixPercent:Int){
          // Fixing the schedule of all lecture and exercise sessions 
          val teachingSlot = (slotExercise.values.flatten ++ slotLecture.values.flatten)
          teachingSlot.map(x =>  add(x == cp.lastSol(x)) )
          
          // Fix group attending exercise course which the deviation of its session is under limited
          for((course,totDiffOfSession) <- deviationOfExerciseSessions){
              val totDiff = cp.lastSol(totDiffOfSession)
              if(totDiff.toFloat/ totStuExerciseSession(course) > margin && rand.nextInt(100) < fixPercent){
                  subgroupExerciseVariable(course).map{x => add(x == cp.lastSol(x)) }
              }
          }
      }
      
      /*
       * Fixed variables when optimizing the total conflict in student schedule
       */
      def fixVar_TotalConflictScheduleOptimization(fixPercent:Int){
//          val noConflictCourses = getCoursesInProgramNonConflict()
          val noConflictCourses =  getCoursesNonConflict()
          // a portion of courses having no conflict are fixed
          for(course <- noConflictCourses; if rand.nextInt(100) < fixPercent ){
              // fixed slot lecture
              slotLecture(course).foreach{ x =>  add(x == cp.lastSol(x)) }
              
              // fixed slot exercise
              if(slotExercise.contains(course) )
                  slotExercise(course).foreach{ x => add(x == cp.lastSol(x)) }
              
              // fixed 
              if(slotPartialSubcriptionExercise.contains(course))
                    subgroupExerciseVariable(course).map{x => add(x == cp.lastSol(x)) }
          }
      }
      
      /*
       * Return set of courses do not overlap with other courses having common students
       */
      private def getCoursesNonConflict(): Array[String] ={
        var noConflictCourses = courses
        
        for(g <- groupStudent; if nbStudent(g) > 0; i <- 0 until nbSubGroups(g)){
            val violation = subGroupViolation(g,i)
            if(cp.lastSol(violation) > 0){
                val conflictInSlots = Map[Int,Set[String]]()
                
                // find the courses causing the conflict
                for(course <- data.program(g)){
                    // get the schedule of lecture sessions
                    for( x <- slotLecture(course) ){
                       val v = cp.lastSol(x)
                       val currCourses = conflictInSlots.getOrElse(v, Set())
                       conflictInSlots.update(v, currCourses + course)
                    }
                    // get the schedule of full subscription sessions
                    if(fullSubscriptionExercise.contains(course)){
                        for( x <- slotExercise(course) ){
                           val v = cp.lastSol(x)
                           val currCourses = conflictInSlots.getOrElse(v, Set())
                           conflictInSlots.update(v, currCourses + course)
                        }
                    }else if(partialSubscriptionExercise.contains(course)){
                        val indexVar = lstGroupObj(g).subGroupUniExercise(i)(course)
                        val index = cp.lastSol(indexVar)
                        val v = cp.lastSol(slotExercise(course)(index))
                        val currCourses = conflictInSlots.getOrElse(v, Set())
                        conflictInSlots.update(v, currCourses + course)
                    }
                }
                
              val conflictCourses =  conflictInSlots.filter{case (v,courseOverp) 
                                                            => courseOverp.size > 1}.values.flatten.toSet
              noConflictCourses =  noConflictCourses --  conflictCourses                                        
            }
        }  
        return noConflictCourses.toArray
      }
      
      /*
       * Return set of courses of program which its subgroups have no conflict in schedule
       */
      private def getCoursesInProgramNonConflict(): Array[String] ={
        var noConflictCourses = courses
        for(g <- groupStudent; if nbStudent(g) > 0){
          var isConflict = false
          for(i <- 0 until nbSubGroups(g); if !isConflict){
            val violation = subGroupViolation(g,i)
            if(cp.lastSol(violation) > 0)
                isConflict = true
          }
          if(isConflict)
              data.program(g).foreach { c =>  noConflictCourses = noConflictCourses - c }
        }
        
        return noConflictCourses.toArray
      }
      def isInLowQualitySlots(s : Int):Boolean = {
               return qualitySlots(s) < 1
      }
      def isInEndDaySlots(s : Int):Boolean = {
               return endDaySlots(s) == 1
      }
      
}